# Documentation for generate_toys
Script to generate and persist N toy datasets in the form of `RooDataSet` in a `RooWorkspace`. Each toy dataset will be stored in a single root file to facilitate distributed fitting of the toy samples.

## Command line options
The options which can be given to the executable are
- `-c` : location of the config file.
- `-d` : working directory (files will be prepended with this directory, optional).
- `-i` : input file (single file containing a `RooAbsPdf` in a `RooWorkspace` (e.g. from a previous fit))
- `-j` : search-replace expressions for the config file (<search_for0:replace_by0;search_for1:replace_by1;...>)
- `-m` : name of the pdf to generate samples from.
- `-o` : output file (note that this can be stub. The script will append an index number internally).
- `-v` : verbosity (1-3, optional)
- `-w` : input `RooWorkspace` name.
- `-h` : help

## Config file
It's possible to use the same config file that was used for fitting. Only the following options will be consumed though:
| name                  | type                   | description                                                                                                                                  | default       |
-------------           |------                  |------------                                                                                                                                  |----------
| `observables`         | (property tree child)  | List of observables                                                                                                                          | - (mandatory) |
| `suppress_RootInfo`   | int                    | Suppresses ROOT messages and RooFit messages below the following levels (-1: unset) DEBUG=0, INFO=1, PROGRESS=2, WARNING=3, ERROR=4, FATAL=5 | optional      |
| `VerboseToys`         | bool                   | Passes `Verbose` `RooCmdArg` to `ToyMCStudy`                                                                                                 | optional      |
| `SilenceToys`         | bool                   | Passes `Silence` `RooCmdArg` to `ToyMCStudy`                                                                                                 | optional      |
| `BinnedToys`          | bool                   | Passes `Binned` `RooCmdArg` to `ToyMCStudy`                                                                                                  | optional      |
| `ExtendedToys`        | bool                   | Passes `Extended` `RooCmdArg` to `ToyMCStudy`. The number of events per toy will be a Poissonian distribution around `NEvtToys`              | optional      |
| `NEvtToys`            | int                    | Number of events to generate for each toy sample. If 0, the number of events will be parsed from the input model.                            | default 0     |

## Snakemake wrapper
The wrapper consumes inputs from the "input.sample" node.
This means that input files have to be created in the workflow; usually by a preceeding fit.

The "_idx.root" suffix that is appended automatically in generate_toys is stripped in this wrapper,
such that output files can be checked by snakemake, and the -o command line option is assembled correctly.

The wrapper needs:
- `input.file` (string): an input file containing a RooWorkspace with RooAbsPdf. Either `file` or the first input is consumed.
- `output` (list): the list of output files generated. Snakemake does not support functions as output files, but we can use multiext and unpacking argument lists while retaining wildcards like this
                 `output = multiext(work_dir+"/toys/{channel}{period}_sel{selection}_fit{fittype}_",*[f"{i}.root" for i in range(0,40)])`
- `params.ntoys` (int): number of toys to generate. recommended to parse from output file list (`ntoys = lambda wildcards, output : int(output[-1].split("_")[-1].strip(".root"))+1`)
Optional arguments:
- `params.work_dir` (str): working directory, default "$RF".
- `params.tee` (str): for logging, default ">".
- `params.workspace_name` (str): RooWorkspace name in input file, default "w".
- `params.model_name` (str): RooAbsPdf name in input file, default "model".
- `params.verbosity` (castable to str)
- `params.<anything>` (str or dict): used to assemble joker string to replace stuff in config file.
