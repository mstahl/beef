# Using approximate_discrete_profiling
This script implements an analytical solution to find the likelihood envelope for the [discrete profiling method](https://inspirehep.net/literature/1312971),
based on approximating the likelihoods with parabolas.

It is assumed that the NLL can be approximated with a (bifurcated) 2nd order polynomial.
The reason for doing this is that we'd like to use profile likelihoods instead of simple ones to account for nuisance parameters;
and calculating reasonably binned profile likelihoods is painfully slow as each bin requires a new minimization/fit.
The trick is to use the NLL at best fit value and MINOS uncertainties to get the polynomial approximation to the profile likelihood
(the reported upper and lower MINOS errors correspond to a delta log likelihood of 0.5).

## Arguments for the executable
- `-d` : work directory
- `-i` : input file name(s) (separated by ;)
- `-o` : ouput file name
- `-m` : model name (the RooAbsPdf with the full fit model)
- `-p` : name of the parameter of interest
- `-j` : axis title of the parameter of interest
- `-n` : (optional) name of the snapshotted NLL value
- `-r` : input RooDataSet name
- `-w` : input RooWorkspace name
- `-v` : verbosity (1-3, optional)
- `-h` : help

## Snakemake wrapper
The wrapper needs:
- `input.sample` or `params.sample` (list or string): input file(s) containing a `RooWorkspace` with `RooAbsPdf`, `RooDataSet` and `RooFitResult`.
- `params.work_dir` (str): working directory, default "$RF".
- `params.tee` (str): for logging, default ">".
- `params.workspace_name` (str): `RooWorkspace` name in input file, default "w".
- `params.dataset_name` (str): `RooDataSet` name in input file, default "ds".
- `params.verbosity` (castable to str).
- `params.poi_name` (str): parameter of interest name (used for fitting).
- `params.model_name` (str): `RooAbsPdf` used for fitting (the full model).
- `params.poi_title` (str): parameter of interest title for the output plot.
- `params.nll_name` (str): name of the `RooRealVar` holding the minimal NLL.