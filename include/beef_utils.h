#ifndef BEEF_UTILS_H
#define BEEF_UTILS_H
//STL
#include <fstream>
#include <cassert>
//ROOT
#include "TEfficiency.h"
#include "TMatrixD.h"
#include "TMatrixDEigen.h"
#include "TRandom3.h"
//RooFit
#include "RooArgSet.h"
#include "RooCategory.h"
#include "RooNumIntConfig.h"
#include "RooSimultaneous.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
//RooStats
#include "RooStats/SPlot.h"
//local
#include <IOjuggler.h>
#include <debug_helpers.h>

#define assertm(exp, msg) assert(((void)msg, exp))
/*!
 *  @addtogroup beef_utils
 *  @{
 */

namespace beef_utils {
namespace pt = boost::property_tree;
/**
  * @fn inline void configure_integrator(const pt::ptree&& iconf)
  * @brief Configure RooFit's integrator
  * @param iconf : property tree for integrator configuration
  * @details Implementation of RooFit's \c IntegratorConfig for tuning the performance of numeric integration (faster vs. more precise).
  *          See [this RooFit-tutorial](https://root.cern/doc/master/rf901__numintconfig_8C_source.html) for some details.
  *          To play with it, you need to give the \c IntegratorConfig node.
  *          Special keywords \c 1D , \c 2D and \c ND set up the types of integrators for pdfs of those dimensionality,
  *          and nodes within \c IntegratorConfig configure those integrators. Here is an example:
  * \code
  *   IntegratorConfig {
  *     1D RooAdaptiveGaussKronrodIntegrator1D
  *     2D RooBinIntegrator
  *     ND RooBinIntegrator
  *     RooBinIntegrator {
  *       numBins 50
  *     }
  *   }
  * \endcode
  */
inline void configure_integrator(const pt::ptree&& iconf) {
  enum dim { anything,
             oneD,
             twoD,
             ND };
  std::map<std::string, dim> dimmap{{"1D", dim::oneD}, {"2D", dim::twoD}, {"ND", dim::ND}};
  auto intConfig = RooAbsReal::defaultIntegratorConfig();
  for (const auto& ic : iconf) {
    switch (dimmap[ic.first.data()]) {
      case dim::oneD:
        intConfig->method1D().setLabel(ic.second.data().data());
        break;
      case dim::twoD:
        intConfig->method2D().setLabel(ic.second.data().data());
        break;
      case dim::ND:
        intConfig->methodND().setLabel(ic.second.data().data());
        break;
      default:
        auto cs = intConfig->getConfigSection(ic.first.data());
        for (const auto& rvic : ic.second)
          cs.setRealValue(rvic.first.data(), std::stod(rvic.second.data()));
        break;
    }
  }
}

/**
  * @fn inline void build_sim_model(RooWorkspace* w, const pt::ptree& configtree, std::string& model_name_for_fit,
  *                                 const MessageService& msgsvc = MessageService("beef_utils::build_sim_model", MSG_LVL::INFO))
  * @brief Build a model for a simultaneous fit.
  * @param w                  : workspace for the current fit
  * @param configtree         : property tree for configuration
  * @param ds                 : dataset to add labels to
  * @param model_name_for_fit : pdf name that will be reset here, and used later to decide which model is used for fitting
  * @param msgsvc             : external or dummy MessageService
  */
inline void build_sim_model(RooWorkspace* w, const pt::ptree& configtree, std::string& model_name_for_fit,
                            const MessageService& msgsvc = MessageService("beef_utils::build_sim_model", MSG_LVL::INFO)) {
  model_name_for_fit = configtree.get("sim_pdf", "sim_model");
  if (const auto sp = configtree.get_optional<std::string>("simultaneous.SplitParam"))
    w->factory(TString::Format("SIMCLONE::%s(%s,%s)", model_name_for_fit.data(), configtree.get<std::string>("pdf").data(), sp->data()).Data());
  else
    throw std::runtime_error("\"simultaneous.SplitParam\" not found");
  if (const auto esp = configtree.get_optional<std::string>("simultaneous.SplitParamEdit"))
    w->factory(esp->data());
}

/**
  * @fn inline void label_dataset(RooWorkspace* w, RooAbsPdf* model, const pt::ptree& configtree, std::string& ds_name_for_fit, const RooDataSet& ds, bool&& nonopt_cut,
  *                               const MessageService& msgsvc = MessageService("beef_utils::label_dataset", MSG_LVL::INFO))
  * @brief Create new dataset adding categories for a simultaneous fit.
  * @param w               : workspace for the current fit
  * @param model           : fit model
  * @param configtree      : property tree for configuration
  * @param ds_name_for_fit : dataset name that will be reset here, and used later to decide which dataset to fit
  * @param nonopt_cut      : flag that determines whether to parse labelling rules from the command line
  * @param selcmdl         : labelling rules from the command line
  * @param msgsvc          : external or dummy MessageService
  */
inline void label_dataset(RooWorkspace* w, RooAbsPdf* model, const pt::ptree& configtree, std::string& ds_name_for_fit, bool nonopt_cut,
                          const std::string&& selcmdl, const MessageService& msgsvc = MessageService("beef_utils::label_dataset", MSG_LVL::INFO)) {
  msgsvc.infomsg("Adding labels/categories to dataset for a simultaneous fit");
  // get the data which is going to be split in categories from the workspace
  const auto ds = IOjuggler::get_wsobj<RooDataSet>(w, ds_name_for_fit);
  // we have the data now and can set the name of the dataset to be fitted to something else
  ds_name_for_fit = configtree.get("dsCombName", "ds_comb");
  auto cat = IOjuggler::get_wsobj<RooCategory>(w, static_cast<RooSimultaneous*>(model)->indexCat().GetName());
  RooDataSet ds_comb(ds_name_for_fit.data(), ds_name_for_fit.data(), RooArgSet(*ds->get(), *cat));

  auto add_cat_and_ds = [&w, &ds_comb, &cat, &ds, &msgsvc](auto&& cat_name, auto&& cut) -> void {
    RooDataSet ds_partial(("ds_"+cat_name).data(), ("ds_"+cat_name).data(), ds, *ds->get(), cut.data());
    cat->setLabel(cat_name.data());
    ds_partial.addColumn(*cat);
    msgsvc.infomsg(TString::Format("Selected %.0f candidates for category \'%s\' with cut %s", ds_partial.sumEntries(), cat_name.data(), cut.data()));
    ds_comb.append(ds_partial);
    w->import(ds_partial);
  };

  if (nonopt_cut) {  //with one cut (from the command line), there are only two possibilities: pass or fail
    add_cat_and_ds(static_cast<std::string>("pass"), selcmdl);
    add_cat_and_ds(static_cast<std::string>("fail"), "!("+selcmdl+")");
  } else
    for (const auto& catn : configtree.get_child("simultaneous"))
      if (catn.first.find("SplitParam") == std::string::npos)
        add_cat_and_ds(catn.first, catn.second.get_value<std::string>());

  w->import(ds_comb);
}

/**
  * @fn inline std::vector<RooCmdArg*> set_fit_options(RooWorkspace* w, const pt::ptree& configtree, RooArgSet& parameters_for_minos,
  *                                                    const MessageService& msgsvc = MessageService("beef_utils::set_fit_options", MSG_LVL::INFO))
  * @brief Build a model for a simultaneous fit.
  * @param w                    : workspace for the current fit
  * @param configtree           : property tree for configuration
  * @param parameters_for_minos : container of variables for running Minos on them
  * @param msgsvc               : external or dummy MessageService
  * @details See the more detailed documentation in the [beef repository](https://gitlab.cern.ch/mstahl/beef/-/blob/master/doc/beef.md#fit-options)
  */
inline std::vector<RooCmdArg*> set_fit_options(RooWorkspace* w, const pt::ptree& configtree, RooArgSet& parameters_for_minos,
                                               const MessageService& msgsvc = MessageService("beef_utils::set_fit_options", MSG_LVL::INFO)) {
  std::vector<RooCmdArg*> args;
  args.push_back(new RooCmdArg(RooFit::Save()));  //we want to save the fitresult, so this is not optional
  if (configtree.get_optional<bool>("fitoptions.Extended")) args.push_back(new RooCmdArg(RooFit::Extended()));
  if (configtree.get_optional<bool>("fitoptions.SumW2Error")) args.push_back(new RooCmdArg(RooFit::SumW2Error(true)));
  if (configtree.get_optional<bool>("fitoptions.InitialHesse")) args.push_back(new RooCmdArg(RooFit::InitialHesse()));
  if (configtree.get_optional<bool>("fitoptions.Verbose")) args.push_back(new RooCmdArg(RooFit::Verbose()));
  if (configtree.get_optional<bool>("fitoptions.WarningsOff")) args.push_back(new RooCmdArg(RooFit::Warnings(false)));
  if (configtree.get_optional<bool>("fitoptions.Timer")) args.push_back(new RooCmdArg(RooFit::Timer()));
  if (configtree.get_optional<bool>("fitoptions.Offset")) args.push_back(new RooCmdArg(RooFit::Offset(true)));
#if ROOT_VERSION_CODE > ROOT_VERSION(6, 19, 0)
  if (configtree.get_optional<bool>("fitoptions.BatchMode")) args.push_back(new RooCmdArg(RooFit::BatchMode(true)));
  if (configtree.get_optional<bool>("fitoptions.EvalErrorWall")) args.push_back(new RooCmdArg(RooFit::EvalErrorWall(false)));
  if (auto opt = configtree.get_optional<double>("fitoptions.PrefitDataFraction"))
    args.push_back(new RooCmdArg(RooFit::PrefitDataFraction(*opt)));
  if (configtree.get_optional<bool>("fitoptions.AsymptoticError"))
    args.push_back(new RooCmdArg(RooFit::AsymptoticError(true)));
#endif
#if ROOT_VERSION_CODE > ROOT_VERSION(6, 24, 0)
  if (auto opt = configtree.get_optional<double>("fitoptions.RecoverFromUndefinedRegions"))
    args.push_back(new RooCmdArg(RooFit::RecoverFromUndefinedRegions(*opt)));
#endif
  if (auto opt = configtree.get_optional<int>("fitoptions.Hesse")) args.push_back(new RooCmdArg(RooFit::Hesse(*opt)));
  if (auto opt = configtree.get_optional<int>("fitoptions.Strategy")) args.push_back(new RooCmdArg(RooFit::Strategy(*opt)));
  if (auto opt = configtree.get_optional<int>("fitoptions.Optimize")) args.push_back(new RooCmdArg(RooFit::Optimize(*opt)));
  if (auto opt = configtree.get_optional<int>("fitoptions.PrintLevel")) args.push_back(new RooCmdArg(RooFit::PrintLevel(*opt)));
  if (auto opt = configtree.get_optional<int>("fitoptions.PrintEvalErrors")) args.push_back(new RooCmdArg(RooFit::PrintEvalErrors(*opt)));
  if (auto opt = configtree.get_optional<std::string>("fitoptions.MinimizerType"))
    args.push_back(new RooCmdArg(RooFit::Minimizer((*opt).data(), configtree.get("fitoptions.MinimizerAlg", "migrad").data())));
  if (auto opt = configtree.get_optional<int>("fitoptions.NumCPU"))
    args.push_back(new RooCmdArg(RooFit::NumCPU(*opt, configtree.get("fitoptions.StratCPU", 0))));
  if (configtree.get_optional<bool>("fitoptions.Minos")) {
    msgsvc.infomsg("Adding all parameters to MINOS");
    args.push_back(new RooCmdArg(RooFit::Minos()));
  } else if (auto pois = configtree.get_child_optional("fitoptions.Minos")) {
    for (const auto& poi : *pois) {
      parameters_for_minos.add(*IOjuggler::get_wsobj<RooAbsArg>(w, poi.first));
      msgsvc.infomsg("Adding "+poi.first+" to MINOS arguments");
    }
    args.push_back(new RooCmdArg(RooFit::Minos(parameters_for_minos)));
  }
  return args;
}

/**
  * @fn inline void make_constraints(RooWorkspace* w, const pt::ptree& configtree, std::vector<TString>& constraints, const std::string& wd, const std::string& wsn,
  *                                  const MessageService& msgsvc = MessageService("beef_utils::make_constraints", MSG_LVL::INFO))
  * @brief build and persist functions for parameter constraints
  * @param w           : workspace for the current fit
  * @param configtree  : property tree for configuration
  * @param constraints : vector of strings that look like "childName:fitresultsFile[:fitresultsWorkspace]"
  * @param wd          : working directory
  * @param wsn         : workspace name
  * @param msgsvc      : external or dummy MessageService
  * @details beef has two options to constrain parameters: directly from the config file, or from a previous fit. this function handles both
  */
inline RooArgSet make_constraints(RooWorkspace* w, const pt::ptree& configtree, std::vector<TString>& constraints, const std::string& wd, const std::string& wsn,
                                  const MessageService& msgsvc = MessageService("beef_utils::make_constraints", MSG_LVL::INFO)) {
  //a set that holds the p.d.f.s that are used to constrain parameters. they will be passed as external constraints to Minuit later
  RooArgSet constrainFuncs;

  //this lambda creates the Gaussians in the current workspace or fixes parameters
  auto constrain_parameter = [&w, &msgsvc, &constrainFuncs](auto&& pn, const double val, const double err = 0.0) -> void {
    auto pdfPar = IOjuggler::get_wsobj<RooAbsReal>(w, pn);
    if (err > 0.0) {      //allow to set starting parameter without constraining if err < 0
      msgsvc.infomsg(TString::Format("Constraining %s to %.5g +- %.5g", pn.data(), val, err));
      w->factory(TString::Format("Gaussian::constr_%s(%s,mean_%s[%.9e],width_%s[%.9e])", pn.data(), pn.data(), pn.data(), val, pn.data(), err).Data());
      constrainFuncs.add(*IOjuggler::get_wsobj<RooAbsArg>(w, "constr_"+pn));
    }
    // FIXME: proper type check
    else if (err == 0.0 && static_cast<TString>(pdfPar->IsA()->GetName()).Contains("RooRealVar")) {
      msgsvc.infomsg(TString::Format("Setting %s to %.5g", pn.data(), val));
      static_cast<RooRealVar*>(pdfPar)->setConstant();
    }
    else if(static_cast<TString>(pdfPar->IsA()->GetName()).Contains("RooRealVar"))
      static_cast<RooRealVar*>(pdfPar)->setVal(val);  //set it as starting parameter
    else msgsvc.warningmsg(TString::Format("Cannot fix or set start parameter of %s, which is of type %s",pn.data(),pdfPar->IsA()->GetName()));
  };

  // constrain fit parameters from values in config file (optional)
  if (auto cps = configtree.get_child_optional("constrainParams"))
    for (auto v : *cps)
      constrain_parameter(v.first, v.second.get<double>("value"), v.second.get("error", 0.0));

  // constrain using a user defined p.d.f
  if (auto ext_consts = configtree.get_child_optional("constraint_functions")){
    msgsvc.infomsg("Constraint functions: ");
    for (const auto& v : *ext_consts) {
      std::string component = v.second.data();
      msgsvc.infomsg("      " + component);
      w->factory(component.c_str());
      constrainFuncs.add(*w->pdf(v.first.data()));// TODO: could parse name from factory command so that that name and the key do not have to match
    }
  }

  // constrain fit parameters from values of previous fits (optional)
  for (auto& ftwc : constraints) {
    const auto setParsNode = configtree.get_child_optional("setParams");
    if (!setParsNode) {
      msgsvc.errormsg("Missing \'setParams\' node in fitconfig. Constraints can't be done...");
      break;
    }

    //parse inputs to constraints from the command line
    TObjArray* extraoptArr = ftwc.Tokenize(":");
    if (extraoptArr->GetEntries() != 2 || extraoptArr->GetEntries() != 3)
      std::runtime_error("Invalid syntax of nonoptions. Try <childName:fitresultsFile[:fitresultsWorkspace]>");

    //this is for mapping parameter names from a previous fit to the ones used in the current fit
    const auto paramsNodeName = "setParams."+static_cast<std::string>(static_cast<TObjString*>(extraoptArr->At(0))->GetString().Data());
    const auto parChild = configtree.get_child_optional(paramsNodeName);
    if (!parChild)
      msgsvc.errormsg("Missing child \'"+paramsNodeName+"\' in setParams child in fitconfig");

    //get the previous fitresult. we need file and workspace
    const auto ftogetname = static_cast<std::string>(static_cast<TObjString*>(extraoptArr->At(1))->GetString().Data());
    msgsvc.infomsg("Constraining or fixing parameters from node \'"+paramsNodeName+"\' and file \'"+ftogetname+"\'");
    const auto wstogetname = extraoptArr->GetEntries() == 3 ? static_cast<std::string>(static_cast<TObjString*>(extraoptArr->At(2))->GetString().Data()) : wsn;
    auto wsToGet = IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(ftogetname, wd), wstogetname);
    if (msgsvc.GetMsgLvl() == MSG_LVL::DEBUG) wsToGet->Print();

    // loop over all parameters to set
    for (const auto& it : *parChild) {
      if (!it.second.data().empty()) {  //check if this is a valid key value pair
        auto varToSet_name = it.first;
        msgsvc.debugmsg("At "+varToSet_name);
        bool fix_param = boost::algorithm::starts_with(varToSet_name, "fix");
        try {
          auto varToGet = IOjuggler::get_wsobj<RooRealVar>(wsToGet, it.second.data());
          if (fix_param)
            constrain_parameter(varToSet_name.erase(0, 3), varToGet->getVal());
          else
            constrain_parameter(varToSet_name, varToGet->getVal(), varToGet->getError());
        } catch (std::exception& e) {
          msgsvc.debugmsg("caught exception: "+static_cast<std::string>(e.what()));
          msgsvc.warningmsg("Skipping "+varToSet_name);
        }
      }
    }
  }
  return constrainFuncs;
}

/**
  * @fn inline RooArgSet get_observables(RooWorkspace* w, RooDataSet* data, const pt::ptree& configtree, const bool& as_hist,
  *                                      const MessageService& msgsvc = MessageService("beef_utils::get_observables", MSG_LVL::INFO))
  * @brief Put observables in a RooArgSet and parse the fit range
  * @param w          : input workspace
  * @param data       : data
  * @param configtree : property tree for configuration
  * @param as_hist    : bin the observables?
  * @param msgsvc     : external or dummy MessageService
  */
inline RooArgSet get_observables(RooWorkspace* w, RooDataSet* data, const pt::ptree& configtree, const bool& as_hist,
                                 const MessageService& msgsvc = MessageService("beef_utils::get_observables", MSG_LVL::INFO)) {
  RooArgSet observables;
  for (const auto& obs : configtree.get_child("observables")) {
    auto myobs = IOjuggler::get_wsobj<RooRealVar>(w, obs.first);
    // set range by hand ...
    if(const auto obs_low = obs.second.get_optional<double>("low"), obs_high = obs.second.get_optional<double>("high"); obs_low && obs_high)
      myobs->setRange(*obs_low,*obs_high);
    else { // ... or check if it makes sense, and adjust if necessary
      double ws_obs_lo = myobs->getMin(), ws_obs_hi = myobs->getMax(), plot_obs_lo, plot_obs_hi;
      //fill plot_obs_lo and plot_obs_hi from RooDataSet limits
      data->getRange(*myobs, plot_obs_lo, plot_obs_hi);
      plot_obs_lo = configtree.get("plots."+obs.first+"plot.low", plot_obs_lo);
      plot_obs_hi = configtree.get("plots."+obs.first+"plot.high", plot_obs_hi);

      if (ws_obs_lo == -std::numeric_limits<double>::infinity() && ws_obs_hi == std::numeric_limits<double>::infinity()) {
        msgsvc.debugmsg("Observable range of "+obs.first+" is infinite");
        myobs->setMin(plot_obs_lo);
        myobs->setMax(plot_obs_hi);
        const auto se_before = data->sumEntries();
        data = static_cast<RooDataSet*>(data->reduce((std::to_string(myobs->getMin())+" <= "+obs.first+" && "+obs.first+" <= "+std::to_string(myobs->getMax())).data()));
        const auto se_after = data->sumEntries();
        msgsvc.infomsg(TString::Format("Parsed range of observable %s to be [%.6g,%.6g]", obs.first.data(), plot_obs_lo, plot_obs_hi));
        if (se_before != se_after)
          msgsvc.infomsg(TString::Format("Adjusting the range rejected %.0f events (%.6g %%)", se_before - se_after, 100 * (1 - se_after / se_before)));
      } else if (const auto epsilon = 0.02, ws_range = ws_obs_hi - ws_obs_lo;
                std::abs(ws_obs_lo - plot_obs_lo) > epsilon * ws_range || std::abs(ws_obs_hi - plot_obs_hi) > epsilon * ws_range) {
        msgsvc.warningmsg("Plotting range and observable range do not match for "+obs.first);
        msgsvc.debugmsg("It could be that the plotting range was parsed from the input data, and you have some empty bins around one of the range limits");
        msgsvc.debugmsg(TString::Format("%.6f vs. %.6f and %.6f vs. %.6f", ws_obs_lo, plot_obs_lo, ws_obs_hi, plot_obs_hi));
      }

      if (as_hist)
        myobs->setBins(configtree.get("plots."+obs.first+"plot.bins", 100));
      observables.add(*myobs);
    }
  }
  return observables;
}

/**
  * @fn inline void print_fitresult(const RooFitResult& fr, const RooArgSet* constraints = nullptr,
  *                                 const MessageService& msgsvc = MessageService("beef_utils::print_fitresult", MSG_LVL::INFO))
  * @brief Print fit-results and highlight parameters that reached their limit within their uncertainties
  * @param fr          : a RooFit fit-result
  * @param constraints : RooArgSet containing those parameters which have been constrained in the fit
  * @param min_nll     : minimized negative log likelihood (needed, when fitting with RooFit::Offset option)
  * @param npars_corr  : correction to likelihood value to take number of parameters into account
  * @param msgsvc      : external or dummy MessageService
  */
inline void print_fitresult(const RooFitResult& fr, const RooArgSet* constraints = nullptr, const double min_nll = 0., const double npars_corr = 0.,
                            const MessageService& msgsvc = MessageService("beef_utils::print_fitresult", MSG_LVL::INFO)) {
  const auto all_fitparameters = static_cast<RooArgSet>(fr.floatParsFinal());
  auto fpit = all_fitparameters.createIterator();
  msgsvc.infomsg(std::string(128, '*'));
  msgsvc.infomsg(TString::Format("* %-124s *", ("Printing "+static_cast<std::string>(fr.GetName())).data()));
  msgsvc.infomsg(TString::Format("* %-146s *", "Best parameter values (color code - parameter at domain limit within \033[1;31m 1 sigma \033[0m, \033[1;33m 3 sigma \033[0m)"));
  msgsvc.infomsg(std::string(128, '*'));
  auto header = TString::Format("%-2s %-20s   %-11s  %-11s  %-11s", "#", "parametername", "value", "error high", "error low");
  const bool has_constraints = constraints != nullptr;
  if (has_constraints) header += TString::Format("  %-15s", "pull constraint");
  msgsvc.infomsg(header);
  unsigned int pcount = 1u;
  double correction_from_constraints = 0.;
  while (auto fp = static_cast<RooRealVar*>(fpit->Next())) {
    const auto fit_val = fp->getVal();
    const auto fit_el = fp->getErrorLo();
    const auto fit_eh = fp->getErrorHi();
    const auto vlo = fp->getMin();
    const auto vhi = fp->getMax();
    auto line = TString::Format("%-2u %-20s = %11.5g  ", pcount, fp->GetName(), fit_val);
    if (fit_val+3 * fit_eh > vhi)
      if (fit_val+fit_eh > vhi)
        line += TString::Format("\033[1;31m%+11.5g\033[0m", fit_eh);
      else
        line += TString::Format("\033[1;33m%+11.5g\033[0m", fit_eh);
    else
      line += TString::Format("%+11.5g", fit_eh);
    if (fit_val+3 * fit_el < vlo)
      if (fit_val+fit_el < vlo)
        line += TString::Format("  \033[1;31m%+11.5g\033[0m", fit_el);
      else
        line += TString::Format("  \033[1;33m%+11.5g\033[0m", fit_el);
    else
      line += TString::Format("  %+11.5g", fit_el);
    if (has_constraints) {
      const std::string parname = fp->GetName();
      if (const auto gc = static_cast<RooGaussian*>(constraints->find(("constr_"+parname).data()))) {
        const auto constr_val = static_cast<RooRealVar*>(gc->getVariables()->find(("mean_"+parname).data()))->getVal();
        const auto constr_err = static_cast<RooRealVar*>(gc->getVariables()->find(("width_"+parname).data()))->getVal();
        const auto pull = (fit_val - constr_val) / constr_err;
        correction_from_constraints += 0.5*fabs(pull);
        if (std::fabs(pull) > 2.)
          if (std::fabs(pull) > 5.)
            line += TString::Format("  \033[1;35m%+11.5g\033[0m", pull);
          else
            line += TString::Format("  \033[1;34m%+11.5g\033[0m", pull);
        else
          line += TString::Format("  %+11.5g", pull);
      }
    }
    msgsvc.infomsg(line);
    pcount++;
  }
  msgsvc.infomsg(std::string(128, '*'));
  const auto the_nll = min_nll == 0. ? fr.minNll() : min_nll;
  msgsvc.infomsg(TString::Format("* %-124s *", TString::Format("Minimized -log(L) = %-.3f (Npars corrected %-.3f, with constraints %-.3f, full %-.3f)",
                 the_nll, the_nll+npars_corr, the_nll+correction_from_constraints, the_nll+npars_corr+correction_from_constraints).Data()));
}

/**
  * @fn inline void print_correlation_matrix(const std::vector<std::vector<double>>& mtrx,
  *                                        const MessageService& msgsvc = MessageService("beef_utils::print_correlation_matrix", MSG_LVL::INFO))
  * @brief Print a matrix to stdout
  * @param mtrx   : correlation matrix
  * @param msgsvc : external or dummy MessageService
  * @details The matrix is printed in color code that highlights entries below -0.5, between -0.2 and -0.5, 0.2 and 0.5 and between 0.5 and 1
  * with different colours to easily spot large correlations. The code however also works with any matrix, you just get a confusing color code
  */
inline void print_correlation_matrix(const std::vector<std::vector<double>>& mtrx,
                                     const MessageService& msgsvc = MessageService("beef_utils::print_correlation_matrix", MSG_LVL::INFO)) {
  msgsvc.infomsg(std::string(128, '*'));
  msgsvc.infomsg(TString::Format("* %-124s *", "Correlations:"));
  msgsvc.infomsg(std::string(128, '*'));
  for (int i = -2; i < static_cast<int>(mtrx.size()); i++) {
    TString line = "";
    for (int j = -1; j < static_cast<int>(mtrx[0].size()); j++) {
      if (i == -2 && j == -1)
        line += "par # |";
      else if (i == -2)
        line += TString::Format("   %-4d", j+1);
      else if (i == -1 && j == -1)
        line += "______|";
      else if (j == -1)
        line += TString::Format("%-6d|", i+1);
      else if (i == -1)
        line += "_______";
      else {
        if (mtrx[i][j] > 0.2 && mtrx[i][j] < 0.5)
          line += TString::Format(" \033[1;33m%-+.3f\033[0m", mtrx[i][j]);
        else if (mtrx[i][j] > 0.5 && mtrx[i][j] < 1.f)
          line += TString::Format(" \033[1;31m%-+.3f\033[0m", mtrx[i][j]);
        else if (mtrx[i][j] < -0.2 && mtrx[i][j] > -0.5)
          line += TString::Format(" \033[1;34m%-+.3f\033[0m", mtrx[i][j]);
        else if (mtrx[i][j] < -0.5)
          line += TString::Format(" \033[1;35m%-+.3f\033[0m", mtrx[i][j]);
        else
          line += TString::Format(" %-+.3f", mtrx[i][j]);
      }
    }
    msgsvc.infomsg(line);
  }
}

/**
  * @fn inline void print_root_correlation_matrix(const TMatrixDSym& mtrx,
  *                                               const MessageService& msgsvc = MessageService("beef_utils::print_root_correlation_matrix", MSG_LVL::INFO))
  * @brief Print a matrix to stdout
  * @param mtrx   : correlation matrix
  * @param msgsvc : external or dummy MessageService
  * @details The matrix is printed in color code that highlights entries below -0.5, between -0.2 and -0.5, 0.2 and 0.5 and between 0.5 and 1
  * with different colours to easily spot large correlations
  */
inline void print_root_correlation_matrix(const TMatrixDSym& mtrx,
                                          const MessageService& msgsvc = MessageService("beef_utils::print_root_correlation_matrix", MSG_LVL::INFO)) {
  msgsvc.infomsg(std::string(128, '*'));
  msgsvc.infomsg(TString::Format("* %-124s *", "Correlation matrix:"));
  msgsvc.infomsg(std::string(128, '*'));
  for (int i = -2; i < mtrx.GetNcols(); i++) {
    TString line = "";
    for (int j = -1; j < mtrx.GetNcols(); j++) {
      if (i == -2 && j == -1)
        line += "par # |";
      else if (i == -2)
        line += TString::Format("   %-4d", j+1);
      else if (i == -1 && j == -1)
        line += "______|";
      else if (j == -1)
        line += TString::Format("%-6d|", i+1);
      else if (i == -1)
        line += "_______";
      else {
        if (mtrx(i, j) > 0.2 && mtrx(i, j) < 0.5)
          line += TString::Format(" \033[1;33m%-+.3f\033[0m", mtrx(i, j));
        else if (mtrx(i, j) > 0.5 && mtrx(i, j) < 1.f)
          line += TString::Format(" \033[1;31m%-+.3f\033[0m", mtrx(i, j));
        else if (mtrx(i, j) < -0.2 && mtrx(i, j) > -0.5)
          line += TString::Format(" \033[1;34m%-+.3f\033[0m", mtrx(i, j));
        else if (mtrx(i, j) < -0.5)
          line += TString::Format(" \033[1;35m%-+.3f\033[0m", mtrx(i, j));
        else
          line += TString::Format(" %-+.3f", mtrx(i, j));
      }
    }
    msgsvc.infomsg(line);
  }
  msgsvc.infomsg(std::string(128, '*'));
}

/**
  * @fn inline void get_quantiles(const std::string& workdir, const RooWorkspace& ws, const pt::ptree&& qnode, const std::string& outfilename, pt::ptree& ptfr,
  *                               const MessageService& msgsvc = MessageService("beef_utils::get_quantiles", MSG_LVL::INFO))
  * @brief get quantiles for a pdf
  * @param ws          : output workspace
  * @param configtree  : property tree for configuration
  * @param outfilename : for default solution if ouput file name is not explicitly configured
  * @param ptfr       : output property tree
  * @param msgsvc      : external or dummy MessageService
  * @details writes quantiles for pdf as plain ascii file to disk and persists cumulative distribution in output workspace. configure like this:
  * \code
  *   getQuantiles {
  *     pdf1 {
  *       observable mD0
  *       low        1822
  *       high       1912
  *       alphas {
  *         0.01
  *         0.025
  *         ...
  *       }
  *       ; alpha      0.025 ; alternative to alphas node
  *       oneSided   1
  *       output     /path/to/output
  *     }
  *     pdf2 {
  *       ...
  *     }
  *   }
  * \endcode
  */
inline void get_quantiles(const std::string& workdir, const RooWorkspace& ws, const pt::ptree&& qnode, const std::string& outfilename, pt::ptree& ptfr,
                          const MessageService& msgsvc = MessageService("beef_utils::get_quantiles", MSG_LVL::INFO)) {
  // loop configs (different models/observables)
  for (const auto& qconf : qnode) {
    // configuration
    const auto pdf_name = qconf.first;
    const auto obs_name = qconf.second.get<std::string>("observable");
    const auto obs = ws.var(obs_name.data());
    const auto range_low = qconf.second.get<double>("low", obs->getMin());
    const auto range_high = qconf.second.get<double>("high", obs->getMax());
    const auto alpha = qconf.second.get_optional<double>("alpha");
    const auto alphas = qconf.second.get_child_optional("alphas");
    assertm(alpha||alphas, "Did not find quantile alpha(s) in config file");
    const auto one_sided = static_cast<bool>(qconf.second.get<unsigned int>("oneSided",0u));
    // get c.d.f.
    msgsvc.infomsg("Creating cumulative distribution function for "+pdf_name+" as function of "+obs_name);
    const auto cdf = ws.pdf(pdf_name.data())->createCdf(RooArgSet(*obs)); // this may take a bit of time for complicated models.
    // container for the quantiles that faciliates config and output
    std::vector<std::pair<std::string,double>> quantile_dump; // TODO: use std::string_view and std::format in the future
    // get the quantiles (roots in the c.d.f)
    auto get_quantile = [&quantile_dump, &cdf, &obs, &range_low, &range_high, &one_sided] (const auto&& alpha) -> void {
      quantile_dump.emplace_back(TString::Format("%.3g",alpha*100).Data(), cdf->findRoot(*obs, range_low, range_high, alpha));
      if (!one_sided) quantile_dump.emplace_back(TString::Format("%.3g",(1-alpha)*100).Data(), cdf->findRoot(*obs, range_low, range_high, 1.-alpha));
    };
    // loop quantlie levels
    if(alphas)
      for(const auto& alpha : *alphas)
        get_quantile(std::stod(alpha.first));
    else get_quantile(std::move(*alpha));
    // quantile container to outputs (stdout/filesystem)
    const auto output_file = workdir+"/"+qconf.second.get<std::string>("output", boost::algorithm::replace_all_copy(outfilename, ".root", "")+"_"+pdf_name+"_"+obs_name);
    std::ofstream output_stream;
    output_stream.open(output_file);
    for(unsigned i=0; i<quantile_dump.size(); i+=one_sided?1u:2u){
      output_stream << quantile_dump[i].first.data() << quantile_dump[i].second << std::endl;
      ptfr.add(pdf_name+"_"+obs_name+"_"+boost::algorithm::replace_all_copy(quantile_dump[i].first, ".", "p")+"_percentile", quantile_dump[i].second);
      if(!one_sided){
        output_stream << quantile_dump[i+1].first.data() << quantile_dump[i+1].second << std::endl;
        ptfr.add(pdf_name+"_"+obs_name+"_"+boost::algorithm::replace_all_copy(quantile_dump[i+1].first, ".", "p")+"_percentile", quantile_dump[i+1].second);
        msgsvc.infomsg(TString::Format("Two-sided %s %% quantile of %s in %s: [%.6g, %.6g]", quantile_dump[i].first.data(), pdf_name.data(), obs_name.data(),
                                        quantile_dump[i].second, quantile_dump[i+1].second));
      }
      else msgsvc.infomsg(TString::Format("One-sided %.6g %% quantile of %s in %s: %.6g",
                                           quantile_dump[i].first.data(), pdf_name.data(), obs_name.data(), quantile_dump[i].second));
    }
    output_stream.close();
    msgsvc.infomsg("Wrote quantiles to "+output_file);
  }
}

/**
  * @fn get_entries_in_range(const std::string& workdir, const pt::ptree&& cutnode, const std::string& outfilename, pt::ptree& ptfr, RooDataSet* ds,
  *                          const MessageService& msgsvc = MessageService("beef_utils::get_entries_in_range", MSG_LVL::INFO))
  * @brief gets the number of entries (actually the sum of weights) in a range defined by "cut"
  * @param workdir     : working directory (as in beef -- for the output to disk)
  * @param cutnode     : defines the ranges
  * @param outfilename : for default solution if ouput file name is not explicitly configured
  * @param ptfr        : output property tree
  * @param ds          : pointer to the dataset to get the entries from
  * @param msgsvc      : external or dummy MessageService
  * @details gets the sum of weights in a range defined by \c cut and saves it as plain ascii file to disk. configure like this:
  * \code
  *   getEntriesInRange {
  *     charm_signal "1830<mD0 && mD0<1880 && 2250<mLc && mLc<2310"
  *     another_cut  "5350<mLb && mLb<5550"
  *     ...
  *   }
  * \endcode
  */
inline void get_entries_in_range(const std::string& workdir, const pt::ptree&& cutnode, const std::string& outfilename, pt::ptree& ptfr, RooDataSet* ds,
                                 const MessageService& msgsvc = MessageService("beef_utils::get_entries_in_range", MSG_LVL::INFO)) {
  // loop configs (different models/observables)
  for (const auto& cutconf : cutnode) {
    const auto range_name = cutconf.first;
    // integral to outputs (stdout/filesystem)
    const auto output_file = workdir+"/"+cutconf.second.get<std::string>("output", boost::algorithm::replace_all_copy(outfilename, ".root", "")+"_"+range_name);
    std::ofstream output_stream;
    output_stream.open(output_file);
    const auto nentries = ds->sumEntries(cutconf.second.data().data());
    output_stream << nentries << std::endl;
    ptfr.add(range_name, nentries);
    msgsvc.infomsg(TString::Format("Sum of weights for range %s: %.6g", range_name.data(), nentries));
    // cleanup and move on
    output_stream.close();
    msgsvc.infomsg("Wrote sum of weights to "+output_file);
  }
}

/**
  * @fn get_integral(const std::string& workdir, const RooWorkspace& ws, const pt::ptree&& inode, const std::string& outfilename, pt::ptree& ptfr
  *                  const MessageService& msgsvc = MessageService("beef_utils::get_integral", MSG_LVL::INFO))
  * @brief get integral for a pdf
  * @param ws          : output workspace
  * @param inode       : configures the calculation
  * @param outfilename : for default solution if ouput file name is not explicitly configured
  * @param ptfr        : output property tree
  * @param msgsvc      : external or dummy MessageService
  * @details writes integral over pdf as plain ascii file to disk. configure like this:
  * \code
  *   getIntegral {
  *     mysignal { ; pdf name
  *       observables {
  *         mD0 { ; observable name
  *           low  1830 ; range for integration
  *           high 1880
  *         }
  *         mLc {
  *           low  2250
  *           high 2310
  *         }
  *       }
  *       RangeName "mysignal_charm_signal_window"
  *       norm       NSig ; normalize the output to some extended parameter
  *       output     /path/to/output
  *     }
  *     another_pdf {
  *       ...
  *     }
  *   }
  * \endcode
  */
inline void get_integral(const std::string& workdir, const RooWorkspace& ws, const pt::ptree&& inode, const std::string& outfilename, pt::ptree& ptfr,
                               const MessageService& msgsvc = MessageService("beef_utils::get_integral", MSG_LVL::INFO)) {
  // ranges need names, and we don't want to mix them up, so we simply increment a helper variable
  unsigned short range_helper = 0;
  // loop configs (different models/observables)
  for (const auto& iconf : inode) {
    // set integration range of this observable and put it in a temporary container (RooArgSet)
    const auto pdf_name = iconf.first;
    const auto rangename = iconf.second.get<std::string>("RangeName",pdf_name+"_integral_range_"+std::to_string(range_helper));
    RooArgSet observables;
    for(const auto& obs : iconf.second.get_child("observables")){
      auto myobs = IOjuggler::get_wsobj<RooRealVar>(&ws, obs.first);
      // small function to read range limits previously written to outputs (like range limits from quantiles)
      auto parse_range = [&ptfr] (const std::string& range_key) -> double {
        if(std::find_if(range_key.begin(), range_key.end(), [](char c){ return !(std::isdigit(c) || c == '.');}) == range_key.end())
          return std::stod(range_key); // if the string is a number, just cast it to a double
        else return ptfr.get<double>(range_key); // if not, get the number from the output info file
      };
      if(const auto low_key = obs.second.get_optional<std::string>("low"), high_key = obs.second.get_optional<std::string>("high"); low_key && high_key){
        const auto obs_low = parse_range(*low_key), obs_high = parse_range(*high_key);
        assertm(obs_low > myobs->getMin() && obs_high < myobs->getMax(), "limits for integration out of observable domain");
        myobs->setRange(rangename.data(),obs_low,obs_high);
        observables.add(*myobs);
      }
    }
    // get the integral
    msgsvc.infomsg("Calculating integral for "+pdf_name);
    // this may take a bit of time for complicated models.
    auto integral_rar = IOjuggler::get_wsobj<RooAbsPdf>(&ws, pdf_name)->createIntegral(observables, RooFit::NormSet(observables), RooFit::Range(rangename.data()));
    // integral to outputs (stdout/filesystem)
    const auto output_file = workdir+"/"+iconf.second.get<std::string>("output", boost::algorithm::replace_all_copy(outfilename, ".root", "")+rangename);
    std::ofstream output_stream;
    output_stream.open(output_file);
    auto do_it = [&msgsvc, &output_stream, &ptfr, &rangename, &pdf_name] (double&& number) -> void {
      output_stream << number << std::endl;
      ptfr.add(rangename, number);
      msgsvc.infomsg(rangename+" integral "+std::to_string(number));
    };
    if(const auto norm_var = iconf.second.get_optional<std::string>("norm")) do_it(IOjuggler::get_wsobj<RooRealVar>(&ws, *norm_var)->getVal()*integral_rar->getVal());
    else do_it(integral_rar->getVal());
    // cleanup and move on
    output_stream.close();
    msgsvc.infomsg("Wrote integral to "+output_file);
    range_helper++;
  }
}

/**
  * @fn inline void save_fitresult_as_info(RooWorkspace& ws, RooFitResult* fr, const pt::ptree& configtree, pt::ptree& ptfr,
  *                                        const MessageService& msgsvc = MessageService("beef_utils::save_fitresult_as_info", MSG_LVL::INFO))
  * @brief write fitresult as info file to disk
  * @param ws         : output workspace
  * @param fr         : fit result
  * @param configtree : property tree for configuration
  * @param ptfr       : output property tree
  * @param msgsvc     : external or dummy MessageService
  */
inline void save_fitresult_as_info(RooWorkspace& ws, const RooFitResult* fr, const pt::ptree& configtree, pt::ptree& ptfr,
                                   const MessageService& msgsvc = MessageService("beef_utils::save_fitresult_as_info", MSG_LVL::INFO)) {
  const auto infonode = configtree.get_child_optional("saveFitResultsAsInfo");  //also true if it's only a key-value pair
  RooArgSet params(fr->floatParsFinal(), fr->constPars());
  params.add(ws.allFunctions());
  auto pars_for_correlations = fr->floatParsFinal();
  const bool save_corr = static_cast<bool>(configtree.get("saveCorrelationsAsInfo", 0));
  auto write_param_to_info = [&ptfr, &fr, &pars_for_correlations, &save_corr](const auto& param) {
    if (param == nullptr || !param->InheritsFrom("RooAbsReal"))
      throw std::runtime_error("param is nullptr or doesn't inherit from RooAbsReal");
    const std::string varname = param->GetName();
    ptfr.add(varname+".Value", static_cast<RooAbsReal*>(param)->getVal());
    if (param->InheritsFrom("RooRealVar")) {
      auto p = static_cast<RooRealVar*>(param);
      ptfr.add(varname+".Error", p->getError());
      if (p->hasAsymError()) {
        ptfr.add(varname+".ErrorLo", p->getErrorLo());
        ptfr.add(varname+".ErrorHi", p->getErrorHi());
      }
      if (save_corr && pars_for_correlations.contains(*p)) {
        pars_for_correlations.remove(*p);  //don't run twice and diagonal
        auto cpit = pars_for_correlations.createIterator();
        while (auto vfr = cpit->Next())
          ptfr.add("Corr_"+varname+"_"+vfr->GetName(), fr->correlation(*p, *static_cast<RooRealVar*>(vfr)));
      }
    } else ptfr.add(varname+".Error", static_cast<RooAbsReal*>(param)->getPropagatedError(*fr));
  };
  if (configtree.get("saveFitResultsAsInfo", 0)) {  //false if it's a "real" ptree child
    auto fpit = params.createIterator();
    while (auto var_fitres = fpit->Next())
      write_param_to_info(var_fitres);
  } else if (infonode) {
    for (const auto& infoit : *infonode) {
      try {
        write_param_to_info(params.find(infoit.first.data()));
      } catch (std::exception& e) {
        msgsvc.warningmsg("caught exception "+static_cast<std::string>(e.what()));
        msgsvc.warningmsg("can't find "+infoit.first+" in fitresult to add it to tex output. skipping it...");
      }
    }
  }
}

/**
  * @fn inline void save_fitresult_as_tex(RooWorkspace& ws, const pt::ptree& configtree, const std::string& outfilename,
  *                               const MessageService& msgsvc = MessageService("beef_utils::save_fitresult_as_tex", MSG_LVL::INFO))
  * @brief get quantiles for a pdf
  * @param ws          : output workspace
  * @param configtree  : property tree for configuration
  * @param outfilename : for default solution if ouput file name is not explicitly configured
  * @param msgsvc      : external or dummy MessageService
  * @details writes quantiles for pdf as plain ascii file to disk and persists cumulative distribution in output workspace.
  */
inline void save_fitresult_as_tex(RooWorkspace& ws, RooFitResult* fr, const pt::ptree& configtree, const std::string& outfilename,
                                  const MessageService& msgsvc = MessageService("beef_utils::save_fitresult_as_tex", MSG_LVL::INFO)) {
  auto TeXnode = configtree.get_child_optional("saveFitResultsAsTeX");
  //stream for the fit results defined as Tex constants
  auto texout = boost::algorithm::replace_all_copy(outfilename, ".root", ".tex");
  std::ofstream out_tex;
  msgsvc.infomsg("Writing the fit results as TeX constants to "+texout);
  out_tex.open(texout);

  //stream for the fit results defined as Tex constants
  auto texout_table = boost::algorithm::replace_all_copy(outfilename, ".root", "_table.tex");
  std::ofstream out_tex_table;
  msgsvc.infomsg("Writing the fit results as TeX table to "+texout_table);
  out_tex_table.open(texout_table);
  out_tex_table << "\\begin{table}[h]" << std::endl;
  out_tex_table << "\\begin{center}" << std::endl;
  //two columns: fit parameters, and fitted values
  out_tex_table << "\\begin{tabular}{c | c}";
  out_tex_table << "\\toprule" << std::endl;
  //labels of the columns
  out_tex_table << " Fit parameter & Fitted value \\\\" << std::endl;
  out_tex_table << "\\midrule" << std::endl;

  RooArgSet params(fr->floatParsFinal(), fr->constPars());
  params.add(ws.allFunctions());
  // to enable the writing of the correlations
  auto pars_for_correlations = fr->floatParsFinal();
  const bool save_corr = static_cast<bool>(configtree.get("saveCorrelationsAsTex", 1));
  std::map<std::string, std::map<std::string, double>> correlation_matrix;

  auto write_param_to_tex = [&out_tex, &out_tex_table, &fr, &pars_for_correlations, &save_corr, &configtree, &correlation_matrix](const auto& param) {
    if (param == nullptr || !param->InheritsFrom("RooAbsReal"))
      throw std::runtime_error("param is nullptr or doesn't inherit from RooAbsReal");
    //get the Tex prefix, and attach it to the variable name
    //this is used to define the fitted values as Tex constants
    auto tex_prefix_formatted = configtree.get<std::string>("TeX_prefix", "");
    auto name = static_cast<std::string>(param->GetName());
    //format the name
    name = IOjuggler::formatTex_todigits(name);
    //format the tex prefix
    tex_prefix_formatted = IOjuggler::formatTex_towords(tex_prefix_formatted);
    auto name_withprefix = tex_prefix_formatted+static_cast<std::string>(param->GetName());
    //format the variable names used to define Tex constants
    name_withprefix = IOjuggler::formatTex_towords(name_withprefix);
    //value of the parameter
    double value_param = static_cast<RooAbsReal*>(param)->getVal();
    if (param->InheritsFrom("RooRealVar")) {
      auto p = static_cast<RooRealVar*>(param);
      //round the error to the two most significative digits
      double error = p->getError();
      double factor = 1.;
      if (error != 0.) {  // otherwise it will return 'nan' due to the log10() of zero
        factor = pow(10.0, 2 - std::ceil(log10(fabs(error))));
        error = std::round(error * factor) / factor;
      }
      //round the value of the parameter accordingly to the uncertainty
      value_param = std::round(value_param * factor) / factor;
      //write the fit results as Tex constants, with the errors
      out_tex << "\\def \\" << name_withprefix << "{" << value_param << "}" << std::endl;
      out_tex << "\\def \\" << name_withprefix << "Error{" << error << "}" << std::endl;
      if (p->hasAsymError()) {
        out_tex << "\\def \\" << name_withprefix << "ErrorLo{" << p->getErrorLo() << "}" << std::endl;
        out_tex << "\\def \\" << name_withprefix << "ErrorHi{" << p->getErrorHi() << "}" << std::endl;
      }
      //write the fit results and errors as Tex table
      //skip the parameters which were kept fixed, resulting in 0. error
      if (error != 0.) {
        if (!p->hasAsymError())
          out_tex_table << name << " & " << value_param << " $\\pm$ " << error << " \\\\" << std::endl;
        else
          out_tex_table << name << " & " << value_param
                        << " $^{+" << p->getErrorHi() << "}_{-" << p->getErrorLo() << "}$ \\\\" << std::endl;
      }

      //write the correlations
      if (save_corr && pars_for_correlations.contains(*p)) {
        pars_for_correlations.remove(*p);  //don't run twice and diagonal
        auto cpit = pars_for_correlations.createIterator();
        while (auto vfr = cpit->Next()) {
          //add the first variable to the correlation matrix
          std::map<std::string, double> temp;
          //remove from the variable name the TeX prefix
          std::string name_nicer = name_withprefix;
          boost::replace_all(name_nicer, tex_prefix_formatted, "");
          correlation_matrix.insert(std::pair<std::string, std::map<std::string, double>>(name_nicer, temp));
          std::string name_2 = vfr->GetName();
          //format the variable name
          name_2 = IOjuggler::formatTex_towords(name_2);
          //round the up to the two most significant digits
          double correlation = fr->correlation(*p, *static_cast<RooRealVar*>(vfr));
          if (correlation != 0.) {  // otherwise it will return 'nan' due to the log10() of zero
            double factor = pow(10.0, 2 - std::ceil(log10(fabs(correlation))));
            correlation = std::round(correlation * factor) / factor;
          }
          out_tex << "\\def \\" << (name_withprefix+"Corr"+name_2) << "{" << correlation << "}" << std::endl;
          //remove from the variable name the TeX prefix
          std::string name_2_nicer = name_2;
          boost::replace_all(name_2_nicer, tex_prefix_formatted, "");
          name_2_nicer = IOjuggler::formatTex_todigits(name_2_nicer);
          //add the second variable to the matrix, with the correlation wrt the first variable
          correlation_matrix[name_nicer].insert(std::pair<std::string, double>(name_2_nicer, correlation));
          //just to be sure, fill also the symmetric way otherwise the last parameter won't be printed
          correlation_matrix.insert(std::pair<std::string, std::map<std::string, double>>(name_2_nicer, temp));
          correlation_matrix[name_2_nicer].insert(std::pair<std::string, double>(name_nicer, correlation));
        }
      }  //write the correlations
    } else {
      out_tex << "\\def \\" << name_withprefix << "{" << value_param << "}" << std::endl;
      out_tex << "\\def \\" << name_withprefix << "Error{" << static_cast<RooAbsReal*>(param)->getPropagatedError(*fr) << "}" << std::endl;
    }
  };

  if (configtree.get("saveFitResultsAsTeX", 0)) {  //false if it's a "real" ptree child
    auto fpit = params.createIterator();
    while (auto var_fitres = fpit->Next())
      write_param_to_tex(var_fitres);
  } else if (TeXnode) {
    for (const auto& TeXit : *TeXnode) {
      try {
        write_param_to_tex(params.find(TeXit.first.data()));
      } catch (std::exception& e) {
        msgsvc.warningmsg("caught exception "+static_cast<std::string>(e.what()));
        msgsvc.warningmsg("can't find "+TeXit.first+" in fitresult to add it to tex output. skipping it...");
      }
    }
  }
  out_tex_table << "\\bottomrule" << std::endl;
  out_tex_table << "\\end{tabular}" << std::endl;
  out_tex_table << "\\end{center}" << std::endl;
  out_tex_table << "\\caption{A nice table created automatically by beef.}" << std::endl;
  out_tex_table << "\\label{tab:fit_results}" << std::endl;
  out_tex_table << "\\end{table}" << std::endl;
  //close the tex streams
  out_tex.close();
  out_tex_table.close();

  //now write out the correlation matrix
  std::ofstream out_corrmatrix_tex;
  //format the output file name
  auto texout_correlations_matrix = boost::algorithm::replace_all_copy(outfilename, ".root", "_corrmatrix.tex");
  msgsvc.infomsg("Writing the correlation matrix as TeX table to "+texout_correlations_matrix);
  out_corrmatrix_tex.open(texout_correlations_matrix);
  //let's open the table
  out_corrmatrix_tex << "\\begin{table}[h]" << std::endl;
  //you want to resize the tables: I put the command there but I comment it
  //uncomment it directly in the Tex if you really need that
  out_corrmatrix_tex << "%\\resizebox{1.5\textwidth}{!}{" << std::endl;
  out_corrmatrix_tex << "\\begin{center}" << std::endl;
  out_corrmatrix_tex << "\\begin{tabular}{c |";
  //I have to add the specify number of columns = number of parameters
  for (const auto& it_param : correlation_matrix)
    out_corrmatrix_tex << " c";
  out_corrmatrix_tex << "}" << std::endl;
  out_corrmatrix_tex << "\\toprule" << std::endl;

  //now it's time for the name of the parameters,
  //to be placed on top of the table
  for (const auto& it_param : correlation_matrix)
    out_corrmatrix_tex << " & " << IOjuggler::formatTex_todigits(it_param.first);
  out_corrmatrix_tex << " \\\\" << std::endl;
  out_corrmatrix_tex << "\\midrule" << std::endl;

  //loop over the parameters stored in the correlation matrix
  for (const auto& it_first : correlation_matrix) {
    //get the name of the current variable
    std::string varname_1 = it_first.first;
    out_corrmatrix_tex << IOjuggler::formatTex_todigits(varname_1) << "\t & ";
    //loop over all the parameters!
    for (const auto& it_second : correlation_matrix) {
      std::string varname_2 = it_second.first;
      //correlation of the variable with itself? Put it to 1 and go to the next variable.
      //Otherwise retrieve it from the map, and continue
      if (varname_1 == varname_2) {
        out_corrmatrix_tex << "1. \\\\" << std::endl;
        break;
      } else {
        const auto correlation = correlation_matrix[varname_1][varname_2];
        out_corrmatrix_tex << correlation << " & ";
      }
    }  //loop over the second variables
  }    //loop over the first variables
  //let's close the table
  out_corrmatrix_tex << "\\bottomrule" << std::endl;
  out_corrmatrix_tex << "\\end{tabular}" << std::endl;
  out_corrmatrix_tex << "\\end{center}" << std::endl;
  //this is related to the resizebox command (see above), commented by default
  out_corrmatrix_tex << "%}" << std::endl;
  out_corrmatrix_tex << "\\caption{A nice huge table created automatically by the beef.}" << std::endl;
  out_corrmatrix_tex << "\\label{tab:correlations}" << std::endl;
  out_corrmatrix_tex << "\\end{table}" << std::endl;
  out_corrmatrix_tex.close();
}

/**
  * @fn inline void calculate_efficiencies(RooWorkspace& ws, const pt::ptree&& effconf, pt::ptree& ptfr,
  *                                        const MessageService& msgsvc = MessageService("beef_utils::calculate_efficiencies", MSG_LVL::INFO))
  * @brief Calculate efficiencies and write them as info file to disk
  * @param ws         : output workspace
  * @param effconf    : property tree for configuration
  * @param ptfr       : output property tree
  * @param msgsvc     : external or dummy MessageService
  * @details Performs calculations as detailed in [CERN-THESIS-2018-176](https://inspirehep.net/literature/1714999) Appendix A. A full configuration looks like
  * \code
  *   efficiency {
  *     NSigPassName   "NSig_pass"    ; (string,default: "NSig_pass") Name of parameter from which to compute efficiency (pass category)
  *     NSigPassName   "NSigFailName" ; (string,default: "NSigFailName") Name of parameter from which to compute efficiency (fail category)
  *     NBkgPassName   "NBkg_pass"    ; (string, optional) Allows to compute background efficiency and save it to output workspace
  *     NBkgFailName   "NBkg_fail"    ; see above
  *     get_all_limits 1              ; (bool, default false) Print (to stdout) and save (to .info) all possible limits provided by TEfficiency and the Wald approximation
  *     StatisticsOption 2            ; (int, default: 0) Choose [TEfficiency::EStatOption](https://root.cern.ch/doc/master/classTEfficiency.html#af27fb4e93a1b16ed7a5b593398f86312)
  *     ConfidenceLevel  0.95         ; (double, default: 0.6827) Choose TEfficiency CL
  *     alpha            0.6          ; (double, default 0.5) For Beta-Function of Bayesian Methods in TEfficiency
  *     beta             0.4          ; (double, default 0.5) For Beta-Function of Bayesian Methods in TEfficiency
  *   }
  * \endcode
  * more details are found in the docs [here](https://gitlab.cern.ch/mstahl/beef/-/blob/master/doc/beef.md#simultaneous-fits)
  */
inline void calculate_efficiencies(RooWorkspace& ws, const pt::ptree&& effconf, pt::ptree& ptfr,
                                   const MessageService& msgsvc = MessageService("beef_utils::calculate_efficiencies", MSG_LVL::INFO)) {
  //calculate the efficiency NB. mstahl: changed lines below in Nov 2020 from w to &ws
  const auto Nsig_pass_var = IOjuggler::get_wsobj<RooRealVar>(&ws, effconf.get("NSigPassName", "NSig_pass"));
  const auto Nsig_fail_var = IOjuggler::get_wsobj<RooRealVar>(&ws, effconf.get("NSigFailName", "NSig_fail"));
  auto p = Nsig_pass_var->getVal();  // number of measured accepted events
  auto f = Nsig_fail_var->getVal();  // number of measured rejected events
  //p and f should be allowed to fluctuate to negative values
  //push them back to their domain, TEfficiency will return bad results otherwise
  if (p < 0.0) {
    f += p;
    p = 0.0;
  }
  if (f < 0.0) {
    p += f;
    f = 0.0;
  }
  const auto dp = Nsig_pass_var->getError();
  const auto df = Nsig_fail_var->getError();
  //measured efficiency
  const auto eff = p / (p+f);

  // lambda to get binomial signal errors
  auto eo = static_cast<TEfficiency::EStatOption>(effconf.get<int>("StatisticsOption", 0));
  const auto cl = effconf.get<double>("ConfidenceLevel", 0.6827);
  const auto alpha = effconf.get<double>("alpha", 0.5);
  const auto beta = effconf.get<double>("beta", 0.5);
  auto get_efferr = [&eo, &cl, &alpha, &beta, &eff](const double& et, const double& ep, const bool& upper) {
    if (eo == TEfficiency::kFCP)
      return TEfficiency::ClopperPearson(et, ep, cl, upper) - eff;
    else if (eo == TEfficiency::kFNormal)
      return TEfficiency::Normal(et, ep, cl, upper) - eff;
    else if (eo == TEfficiency::kFWilson)
      return TEfficiency::Wilson(et, ep, cl, upper) - eff;
    else if (eo == TEfficiency::kFAC)
      return TEfficiency::AgrestiCoull(et, ep, cl, upper) - eff;
    else if (eo == TEfficiency::kFFC)
      return TEfficiency::FeldmanCousins(et, ep, cl, upper) - eff;
    else if (eo == TEfficiency::kBJeffrey)
      return TEfficiency::Bayesian(et, ep, cl, 0.5, 0.5, upper) - eff;
    else if (eo == TEfficiency::kBUniform)
      return TEfficiency::Bayesian(et, ep, cl, 1., 1., upper) - eff;
    else if (eo == TEfficiency::kBBayesian)
      return TEfficiency::Bayesian(et, ep, cl, alpha, beta, upper) - eff;
    else {
      throw std::range_error(
          "Only valid from 0-7 or in TEfficiency namespace: kFCP, kFNormal, "
          "kFWilson, kFAC, kFFC, kBJeffrey, kBUniform, kBBayesian");
      return -66.6;
    }
  };
  //get confidence interval: calculate uncertainty propagation.
  // account for nuisance parameters with scaling the propagated uncertainty to binomial interval with full statistics
  const auto s1st = std::pow(f * dp, 2) / std::pow(p+f, 4);
  const auto s2nd = std::pow(p * df, 2) / std::pow(p+f, 4);
  const auto s3rd = -2 * p * f * eff * (1 - eff) * (std::pow(dp, 2)+std::pow(df, 2)+2 * eff * (1 - eff) * (p+f)) / std::pow(p+f, 4);
  const auto vareff_prop = s1st+s2nd+s3rd;
  //scaling
  const auto wald_var = eff * (1 - eff) / (p+f);
  const auto wald_scl = wald_var / vareff_prop;
  const auto befl = -get_efferr(p+f, p, false);
  const auto befh = get_efferr(p+f, p, true);
  //use scaling to Wald if it makes sense
  auto scl = 0.f;
  if (0.05 < wald_scl && wald_scl < 20. && 0.667 < befl / befh && befl / befh < 1.5)
    scl = wald_scl;
  else
    scl = eff > 0.5 ? std::pow(befl, 2) / vareff_prop : std::pow(befh, 2) / vareff_prop;
  //get the uncertainties
  const auto deff_lo = -get_efferr(scl * (p+f), scl * p, false);
  const auto deff_hi = get_efferr(scl * (p+f), scl * p, true);
  //print efficiency result
  msgsvc.infomsg(std::string(128, '*'));
  msgsvc.infomsg(TString::Format("* Scaling factor : %.7g,  effective Ntot: %.0f,  effective Npass: %.0f", scl, scl * (p+f), scl * p));
  msgsvc.infomsg(TString::Format("* Signal efficiency = ( %7.5g +%.3f -%.3f ) %%", 100 * eff, 100 * deff_hi, 100 * deff_lo));
  msgsvc.infomsg(std::string(128, '*'));

  //write ptree with results
  ptfr.add("sig_eff.Value", eff);
  ptfr.add("sig_eff.ErrorLo", deff_lo);
  ptfr.add("sig_eff.ErrorHi", deff_hi);

  //if you can't decide which limit you like...
  if (effconf.get("get_all_limits", 0)) {
    std::vector<std::string> statnames = {"ClopperPearson", "Normal", "Wilson", "AgrestiCoull", "FeldmanCousins", "BayesJeffrey", "BayesUniform", "BayesBeta"};
    int statoptenum = 0;
    for (const auto& sn : statnames) {
      eo = static_cast<TEfficiency::EStatOption>(statoptenum++);
      const auto del = -get_efferr(scl * (p+f), scl * p, false);
      const auto deh = get_efferr(scl * (p+f), scl * p, true);
      msgsvc.infomsg(TString::Format("%-19.19s = ( %7.5g +%.3f -%.3f ) %%", sn.data(), 100 * eff, 100 * deh, 100 * del));
      ptfr.add(sn+".ErrorLo", del);
      ptfr.add(sn+".ErrorHi", deh);
    }
    ptfr.add("Wald.Error", std::sqrt(wald_var));
    ptfr.add("Prop.Error", std::sqrt(vareff_prop));
  }

  RooRealVar RCVeff("eff_sig", "eff_sig", eff);  //i have no clue why, but importing a RooConstVar doesnt work here (it worked in other scripts, and yes, i included the header)
  RooRealVar RCVeff_lo("deff_sig_lo", "deff_sig_lo", deff_lo);
  RooRealVar RCVeff_hi("deff_sig_hi", "deff_sig_hi", deff_hi);
  ws.import(RCVeff);
  ws.import(RCVeff_lo);
  ws.import(RCVeff_hi);
  const auto nbkgpname = effconf.get_optional<std::string>("NBkgPassName");
  const auto nbkgfname = effconf.get_optional<std::string>("NBkgFailName");
  if (nbkgpname && nbkgfname) {
    const auto bp = IOjuggler::get_wsobj<RooRealVar>(&ws, *nbkgpname)->getVal();
    const auto bf = IOjuggler::get_wsobj<RooRealVar>(&ws, *nbkgfname)->getVal();
    RooRealVar RCVbeff("eff_bkg", "eff_bkg", bp / (bp+bf));
    ws.import(RCVbeff);
  }
}

/**
  * @fn inline void get_sweights(RooDataSet* data, RooAbsPdf* model, const RooFitResult* fr, const pt::ptree& configtree,
  *                              const MessageService& msgsvc = MessageService("beef_utils::get_sweights", MSG_LVL::INFO))
  * @brief Do an sPlot with the method from RooStats
  * @param data       : data
  * @param model      : fit model
  * @param fr         : fit result
  * @param configtree : property tree for configuration
  * @param msgsvc     : external or dummy MessageService
  */
inline void get_sweights(RooDataSet* data, RooAbsPdf* model, const RooFitResult* fr, const pt::ptree& configtree,
                         const MessageService& msgsvc = MessageService("beef_utils::get_sweights", MSG_LVL::INFO)) {
  std::vector<std::string> exn;
  for (const auto& extpar : configtree.get_child("extended")) exn.push_back(extpar.first);
  //Now we can get the sWeights. we need to set all shape parameters constant and put the yield parameters in a RooArgList
  RooArgList extended_parameters;
  //wtf roofit? why does static_cast<RooArgSet>(fitresult->floatParsFinal()).createIterator() give complete nonsense?
  const auto afps = static_cast<RooArgSet>(fr->floatParsFinal());
  auto fpit = afps.createIterator();
  while (auto fp = static_cast<RooRealVar*>(fpit->Next())) {
    if (std::find_if(exn.begin(), exn.end(), [&fp](const auto& nm) { return nm == fp->GetName(); }) != exn.end()) {
      extended_parameters.add(*fp);
      msgsvc.infomsg("Will calculate sweights for "+static_cast<std::string>(fp->GetName()));
    } else
      fp->setConstant();  //i think RooStats would handle this internally, but rather keep it like this and add some more lines below, since the fit validation has been done with this setting.
  }
  RooStats::SPlot("dummy", "An SPlot", *data, model, extended_parameters);
  //unfreeze the parameters again (see statement 3 lines above)
  fpit->Reset();
  while (auto fp = static_cast<RooRealVar*>(fpit->Next()))
    fp->setConstant(false);
}

/**
  * @fn RooDataSet* SampleParamters(const RooArgSet& params, const TMatrixDSym& covMat,
                                    const unsigned int& ntimes = 1, const unsigned int& seed = 42)
  * @brief Generate parameters by eigendecomposition of their covariance matrix
  * @param params : RooArgSet containing parameters
  * @param covMat : Covariance matrix
  * @param ntimes : Generate set of parameters ntimes
  * @param seed   : seed of random number generator that is used to sample the parameters
  * @return RooDataset of sampled parameters. The caller takes ownership
  * @throws runtime error if dimensions of parameter set and covariance matrix do not match
  * @details \attention Number and order of parameters in RooArgSet and covariance matrix have to match! To get a matching parameter-set and covariance automatically do
  *          \code auto fr = RooAbsPdf::fitTo(...);
  *                auto genpards = CalcGizmo::SampleParamters(static_cast<RooArgSet>(fr->floatParsFinal()),fr->covarianceMatrix());
  *          \endcode
  */
RooDataSet* SampleParamters(const RooArgSet& params, const TMatrixDSym& covMat,
                            const unsigned int& ntimes = 1, const unsigned int& seed = 42){

  //eigendecomposition of covariance matrix -> rotate covariance matrix > sample in uncorrelated parameter space ->  rotate back
  const auto npars = params.getSize();
  if(npars != covMat.GetNcols())
    throw std::runtime_error("dimensions of ArgSet and covariance matrix do not match");

  TMatrixDEigen eMat = TMatrixDEigen(covMat);
  //get a matrix consisting out or eigenvectors (columns)
  const auto AT = eMat.GetEigenVectors();
  //get the transposed version
  TMatrixD A(TMatrixD::kTransposed,AT);
  //get the diagonal cov matrix
  const auto cMatD = A*covMat*AT;

  //random numbers
  TRandom3 rnd(seed);
  RooDataSet* sampled_parameters = new RooDataSet("sp_ds","sampled parameters",params);

  //roofit shit
  RooArgSet new_params;//can't simply copy a RooArgSet, since changing values of parameters in the copied set will change their values in the initial set as well. thanks roofit!
  auto pit = params.createIterator();
  std::vector<double> init_params;
  while(auto np = static_cast<RooRealVar*>(pit->Next())){
    RooRealVar* new_par = new RooRealVar(*np,np->GetName());//next line needs l-value ptr. i have no clue where to delete them...
    new_params.add(*new_par);
    init_params.push_back(np->getVal());
  }

  for(unsigned int i = 0u; i < ntimes; i++){
    //sample in space with diagonal covariance
    std::vector<double> sprime(npars,0.0);
    for(int j = 0; j < npars; j++)
      sprime[j] = rnd.Gaus(0.0,std::sqrt(cMatD(j,j)));

    //rotate back
    auto npit = new_params.createIterator();
    int k = 0;
    while(auto np = static_cast<RooRealVar*>(npit->Next())){
      auto new_val = init_params[k];
      for(int j = 0; j < npars; j++)
        new_val += AT(k,j)*sprime[j];
      np->setVal(new_val);
      k++;
    }
    sampled_parameters->add(new_params);
  }
  return sampled_parameters;
}

}  // namespace beef_utils
#endif
