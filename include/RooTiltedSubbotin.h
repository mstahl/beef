/**
  * @class RooTiltedSubbotin
  * @version 1.0
  * @author Marian Stahl
  * @date  2018-12-25
  * @brief Subbotin p.d.f. with sloped plateau
  * @details Useful for describing flat or slightly tilted partially reconstructed backgrounds.
  */

#ifndef ROOTILTEDSUBBOTIN
#define ROOTILTEDSUBBOTIN

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooAbsReal.h"

class RooTiltedSubbotin : public RooAbsPdf {
public:
  RooTiltedSubbotin() = default;

  /**
   * @fn RooTiltedSubbotin(const char *name, const char *title, RooAbsReal& _x, RooAbsReal& _mu,
                    RooAbsReal& _width, RooAbsReal& _beta, RooAbsReal& _slope)
   * @brief ctor with parameters
   * @param name
   * @param title
   * @param _x : observable
   * @param _mu : mean/median of the plateau (same unit as _x)
   * @param _width : width of the plateau (same unit as _x)
   * @param _beta : shape parameter. Converges to Laplace distribution for @code beta=1 @endcode, to a
   * normal distribution for @code beta=2 @endcode and to a uniform distribution for @code beta->inf @endcode.
   * @param _slope : tilts the plateau
   */
  RooTiltedSubbotin(const char *name, const char *title, RooAbsReal& _x, RooAbsReal& _mu,
                    RooAbsReal& _width, RooAbsReal& _beta, RooAbsReal& _slope);
  /**
   * @fn RooTiltedSubbotin(const RooTiltedSubbotin& other, const char* name=0)
   * @brief copy ctor
   * @param other : p.d.f. to copy
   * @param name : name of the new p.d.f.
   */
  RooTiltedSubbotin(const RooTiltedSubbotin& other, const char* name=0) ;
  /**
   * @fn virtual TObject* clone(const char* newname) const
   * @brief ROOT specific clone function
   * @param newname : name of the new p.d.f.
   * @returns A pointer to the p.d.f. as TObject
   */
  virtual TObject* clone(const char* newname) const { return new RooTiltedSubbotin(*this,newname); }
  inline virtual ~RooTiltedSubbotin() = default;

protected:

  RooRealProxy x;
  RooRealProxy mu;
  RooRealProxy width;
  RooRealProxy beta;
  RooRealProxy slope;

  Double_t evaluate() const;

private:

  ClassDef(RooTiltedSubbotin,1)
};

#endif
