/**
  * @class RooPRHill
  * @version 1.0
  * @author Marian Stahl
  * @date  2019-11-16
  * @brief Reparametrized RooHORNSdini, see https://cds.cern.ch/record/2253246/files/LHCb-ANA-2017-018.pdf for details
  * @details Useful for describing "hilly" partially reconstructed backgrounds. Reparametrized such that kinematic endpoints are calculated from masses in the decay chain.
  * See http://pdg.lbl.gov/2017/reviews/rpp2017-rev-kinematics.pdf eqns 48.23 a and b for the calculation.
  * It enables indirect measurements of resonances that constitute partially reconstructed decays.
  */

#ifndef ROOPRHILL
#define ROOPRHILL

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooAbsReal.h"

class RooPRHill : public RooAbsPdf {
public:
  RooPRHill() = default;
  /**
   * @fn RooPRHill(const char *name, const char *title,
                   RooAbsReal& _m, RooAbsReal& _M, RooAbsReal& _m12, RooAbsReal& _m1, RooAbsReal& _m2, RooAbsReal& _m3,
                   RooAbsReal& _xi, RooAbsReal& _sigma)
   * @brief ctor with parameters
   * @param name
   * @param title
   * @param _m : observable.
   * @param _M: mass of decaying particle.
   * @param _m12: mass of quasi-stable resonance.
   * @param _m1: mass of the particle that has not been reconstructed.
   * @param _m2: mass of first reconstructed decay product.
   * @param _m3: mass of second reconstructed decay product.
   * @param _xi : Relative height of the two peaks (horns). When xi > (<)1, the lower (upper) peak is largest.
   * @param _sigma : Width of the core resolution Gaussian. The core Gaussian is the Gaussian of narrowest width in the overall double Gaussian convolution.
   */
  RooPRHill(const char *name, const char *title,
             RooAbsReal& _m, RooAbsReal& _M, RooAbsReal& _m12, RooAbsReal& _m1, RooAbsReal& _m2, RooAbsReal& _m3,
             RooAbsReal& _xi, RooAbsReal& _sigma);
  /**
   * @fn RooPRHill(const RooPRHill& other, const char* name=0)
   * @brief copy ctor
   * @param other : p.d.f. to copy
   * @param name : name of the new p.d.f.
   */
  RooPRHill(const  RooPRHill& other, const char* name = 0);
  /**
   * @fn virtual TObject* clone(const char* newname) const override
   * @brief ROOT specific clone function
   * @param newname : name of the new p.d.f.
   * @returns A pointer to the p.d.f. as TObject
   */
  virtual TObject* clone(const char* newname) const override { return new  RooPRHill(*this,newname); }
  inline virtual ~ RooPRHill() = default;


protected:
  RooRealProxy m;
  RooRealProxy M;
  RooRealProxy m12;
  RooRealProxy m1;
  RooRealProxy m2;
  RooRealProxy m3;
  RooRealProxy xi;
  RooRealProxy sigma;
  Double_t evaluate() const override;

private:
  ClassDefOverride(RooPRHill,1) //  RooPRHill function PDF
};

#endif
