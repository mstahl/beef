# Contributing and Reusing
This is an open analysis tool. Please have a look at our [guidelines](CONTRIBUTING.md) for contributions. If you want to reuse this code in your own project please note the [reciprocity](RECIPROCITY.md) statement.

# Quick start
## Get code, setup environment and compile
Clone the repository including submodules:
```sh
git clone --recursive ssh://git@gitlab.cern.ch:7999/mstahl/beef.git; cd beef
```

We recommend to setup the environment from LCG views provided by the CERN SFT group on cvmfs. E.g.
```sh
source /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc9-opt/setup.sh
```

Building with 8 cores is as easy as
```sh
cmake -S . -B build; cmake --build build -j 8
```

## Running a first example
Generate some toy data:
```sh
root -b -q src/generateToy.C
```

Create workspace called toyws.root
```sh
build/bin/createWorkspace -i toy.root -c config/config.info -o toyws.root -t toy
```
Have a look at the config file to see how to define more complex workspaces.

Perform the fit defined in `config/fitconfig.info`
```sh
build/bin/beef -c config/fitconfig.info -i toyws.root -o toyfit.root
```

Look at the fitresults (png, C and pdf files are created automatically).
The dataset including sweights, model etc. are saved in the workspace of the output file.
Inspect it with
```
> root -l toyfit.root
root [1] _file0->ls()
root [2] RooWorkspace* w = static_cast<RooWorkspace*>(_file0->Get("w"))
root [3] w->Print()
```
The fitted parameter values can then be seen using
```
root [4] auto fr = static_cast<RooFitResult*>(w->obj("<RooFitResult name>"))
root [5] fr->Print()
```

# Writing config files
To configure `beef`, two options files are needed. <br>
One to set up the [RooWorkspace](https://root.cern.ch/doc/master/classRooWorkspace.html "RooWorkspace") which contains all relevant data, defines the observable, and can be used to setup custom pdfs. <br>
The other config file is parsed when running the fit and producing a plot. <br>
The usage of the options files is documented in [doc/createWorkspace.md](doc/createWorkspace.md) and [doc/beef.md](doc/beef.md).

# Plotting from a previous fit
There are dedicated tools, [beefPlot](src/beefPlot.cpp "beefPlot") and [beefPlots](src/beefPlots.cpp "beefPlots"), that allow to re-create a plot without
having to run the fit again. The latter script can also plot multiple datasets and p.d.f.s from different input files/workspaces.
The scripts are meant for plot-cosmetics and debugging, and is therefore not build by default.
To be able to use it, you either have to call the target manually like `make beefPlot`, or have pass a flag to cmake `-DBEEF_PLOT`.
The tool picks up the output from beef and reads in the fitted pdf and data for plotting. The config file can be the same as the one for fitting.
Command line parameters are the similar to beef's.

# Making sPlots (a.k.a. unfolding fit components)
The standard RooStats tool to make sPlots is integrated in beef and can be executed by using the `extended` node in the beef config file (see [doc/beef.md](doc/beef.md)). <br>
A faster way (in terms of CPU time) to get sWeights is calling the `splot` executable that is documented in [doc/splot.md](doc/splot.md). Advantages are that it returns a `TTree` rather than a `RooDataSet` and that it can be used with large samples (which would otherwise be too large to be converted to `RooDataSet`s) by using only those variables really needed in the fit, and an index, so that the order of the input sample can be retained.

# Snakemake
In many analysis the [`snakemake`](https://snakemake.readthedocs.io/en/stable/) workflow is used to combine many dependent analysis steps.
The running example above is combined to a workflow in the [Snakefile](Snakefile).
After setting up the LCG environment, you can get snakemake for yourself with `python3 -m pip install --user snakemake`.
This only needs to be done once. Note that you need python3 for snakemake.
With a working environment you can run the workflow with
```
snakemake
```
