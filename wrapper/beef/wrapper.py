"""
Snakemake wrapper for beef.
For script documentation see
https://gitlab.cern.ch/mstahl/beef/-/blob/d56ce3e96b9057a62c3ebb205d2742e2d5eb9322/doc/beef.md

The wrapper consumes inputs from the "input.sample" node or from "params.sample".
This is to have hooks for input files that are part of the workflow and those that aren't.

The wrapper needs:
- input.sample or params.sample (list or string): an input file containing a RooWorkspace with RooDataSet.
- params.config (str): config file for fitting.
Optional arguments:
- params.work_dir (str): working directory, default "$RF".
- params.tee (str): for logging, default ">".
- params.workspace_name (str): RooWorkspace name in input file, default "w".
- params.dataset_name (str): RooDataSet name in input file, default "ds".
- params.constraint or input.constraint (str or list): constraint files. Constraints from input.constraint are assembled automatically.
  The identifier for the config file is the file name of the file the contstraints are taken from.
  Constraints from params.constraint are assumed to be assembled correctly already (<identifier:filename.root>).
- params.cut (str) : cutstring
- params.verbosity (castable to str)
- params.<anything> (str or dict): used to assemble joker string to replace stuff in config file.
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
import json
sys.path.append(os.environ["BEEFROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import (get_exe, parse_samples, parse_joker)

# before we do anything, check if the executable is there
exe = get_exe("beef")

samples = snakemake.input.get("sample", None)
constraints = snakemake.input.get("constraint", None)
more_samples = snakemake.params.get("sample", None)
more_constraints = snakemake.params.get("constraint", None)
cfg = snakemake.params.get("config", None)
wd  = snakemake.params.get("work_dir", "$RF")
wn  = snakemake.params.get("workspace_name", "w")
dsn = snakemake.params.get("dataset_name", "ds")
cut = snakemake.params.get("cut", "")
vb  = snakemake.params.get("verbosity", None)
tee = snakemake.params.get("tee", ">")
out = snakemake.output

# make sure that all inputs are what we expect them to be
assert samples is not None or more_samples is not None, "\"input.sample\" or non-workflow input from \"params.sample\" is missing"
ifn = parse_samples(samples,more_samples)

cstr = ""
if constraints is not None:
  if isinstance(constraints,str): cstr += "'"+constraints.split("/")[-1].replace(".root","")+":"+constraints+"' "
  elif isinstance(constraints,list): cstr += " ".join(["'{}:{}'".format(c.split("/")[-1].replace(".root",""),c) for c in constraints])+" "
if more_constraints is not None:
  if isinstance(more_constraints,str): cstr += "'"+more_constraints+"' "
  elif isinstance(more_constraints,list): cstr += "'"+"' '".join(more_constraints)+"' "
if cstr: cstr = cstr[:-1]

assert cfg is not None and isinstance(cfg,str), "\"params.config\" has to be provided and needs to be a string"
assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(wn,str), "\"params.workspace_name\" is expected to be a string"
assert isinstance(dsn,str), "\"params.dataset_name\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"

if vb is not None: vb = "-v"+str(vb)
else : vb = ""

# parse joker arguments
joker = parse_joker(snakemake.params,["config","work_dir","workspace_name","dataset_name","tee","sample","constraint","verbosity"])

# strip work_dir from output file
of = out[0].replace(os.path.expandvars(wd),"")
of = of[1:] if of.startswith("/") else of

if cut: cut = "'"+cut+"'"

# what an awful hack (prior to root 6.26.04 it was possible to have empty categories in a simultaneous fit, but yeah, it somehow doesn't make sense and we can catch it here -- better than in beef directly)
skip_fit = snakemake.params.get("efficiency",None)
if skip_fit is not None:
  ofstub = (wd+"/"+of).replace(".root","")
  with open(os.path.expandvars(ofstub)+".json", "w") as out:
    out.write(json.dumps({ "sig_eff" : {"Value" : skip_fit, "ErrorLo" : 0., "ErrorHi" : 0.}}, indent=2))
  shell(f"touch {os.path.expandvars(ofstub)}.root")
  shell(f"touch {os.path.expandvars(ofstub).replace('fits','plots/efficiency')}_pass.pdf")
else :
  shell("{exe} -c {cfg} -d {wd} -i '{ifn}' -o {of} -r {dsn} -w {wn} {joker} {vb} {cstr} {cut} {tee} {snakemake.log} 2>&1")