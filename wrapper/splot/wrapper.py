"""
Snakemake wrapper for splot.
For script documentation see
https://gitlab.cern.ch/mstahl/beef/-/blob/d56ce3e96b9057a62c3ebb205d2742e2d5eb9322/doc/splot.md

The wrapper consumes inputs from the "input.sample" node or from "params.sample".
This is to have hooks for input files that are part of the workflow and those that aren't.

The wrapper needs:
- input.fitresult or params.fitresult (string): file with fitresults as provided by beef.
- params.extended_params (list): list of extended fit parameters.
Optional arguments:
- input.sample or params.sample (string): an input file containing a RooWorkspace with RooDataSet.
- params.work_dir (str): working directory, default "$RF".
- params.tee (str): for logging, default ">".
- params.workspace_name (str): RooWorkspace name in input file, default "w".
- params.dataset_name (str): RooDataSet name in input file, default "Dred".
- params.model_name (str): RooDataSet name in input file, default "model".
- params.fit_result_name (str): Name of RooFitResult in workspace (default parsed from dataset_name and model_name).
- params.tree_name (str): name of tree in "sample". output tree will be called tree_name+"_sweights"
- params.verbosity (castable to str)
- params.<anything> (str or dict): used to assemble joker string to replace stuff in config file.
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
sys.path.append(os.environ["BEEFROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import (get_exe, parse_samples, parse_joker)

# before we do anything, check if the executable is there
exe = get_exe("splot")

samples         = snakemake.input.get("sample", None)
fitresults      = snakemake.input.get("fitresult", None)
more_samples    = snakemake.params.get("sample", None)
more_fitresults = snakemake.params.get("fitresult", None)
extended_params = snakemake.params.get("extended_params", None)
wd              = snakemake.params.get("work_dir", "$RF")
wn              = snakemake.params.get("workspace_name", "w")
dsn             = snakemake.params.get("dataset_name", "Dred")
mdn             = snakemake.params.get("model_name", "model")
frn             = snakemake.params.get("fit_result_name", None)
tn              = snakemake.params.get("tree_name", "t")
vb              = snakemake.params.get("verbosity", None)
tee             = snakemake.params.get("tee", ">")

# make sure that all inputs are what we expect them to be
assert fitresults is not None or more_fitresults is not None, "\"input.fitresults\" or non-workflow input from \"params.fitresults\" is missing"
ifn = ""
if fitresults:
  assert isinstance(fitresults,str), "\"input.fitresults\" is expected to be a string"
  ifn = fitresults
elif more_fitresults:
  assert isinstance(fitresults,str), "\"params.fitresults\" is expected to be a string"
  ifn = more_fitresults

# for running in indexed mode so that the output can be used as friend tree to the input here
if samples:
  assert isinstance(samples,str), "\"input.samples\" is expected to be a string"
  ifn += ";"+samples
elif more_samples:
  assert isinstance(samples,str), "\"params.samples\" is expected to be a string"
  ifn += ";"+more_samples

# names of extended fit parameters
assert extended_params is not None and isinstance(extended_params,list), "\"params.extended_params\" must be given and is expected to be a list"
eps = " ".join(extended_params)

assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(wn,str), "\"params.workspace_name\" is expected to be a string"
assert isinstance(dsn,str), "\"params.dataset_name\" is expected to be a string"
assert isinstance(mdn,str), "\"params.model_name\" is expected to be a string"
assert isinstance(tn,str), "\"params.tree_name\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"

if vb is not None: vb = "-v"+str(vb)
else : vb = ""

if frn is not None: fr = "-p "+str(frn)
else : fr = ""

shell("{exe} -d {wd} -i '{ifn}' -o {snakemake.output[0]} -m {mdn} -r {dsn} -w {wn} -t {tn} {fr} {vb} {eps} {tee} {snakemake.log} 2>&1")