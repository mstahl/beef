"""
Snakemake wrapper for approximate_discrete_profiling.
For script documentation see
https://gitlab.cern.ch/mstahl/beef/-/blob/update/doc/approximate_discrete_profiling.md

The wrapper needs:
- input.sample or params.sample (list or string): input file(s) containing a RooWorkspace with RooAbsPdf, RooDataSet and RooFitResult.
- params.work_dir (str): working directory, default "$RF".
- params.tee (str): for logging, default ">".
- params.workspace_name (str): RooWorkspace name in input file, default "w".
- params.dataset_name (str): RooDataSet name in input file, default "ds".
- params.verbosity (castable to str).
- params.poi_name (str): parameter of interest name (used for fitting).
- params.model_name (str): RooAbsPdf used for fitting (the full model).
- params.poi_title (str): parameter of interest title for the output plot.
- params.nll_name (str): name of the RooRealVar holding the minimal NLL.
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
import json
sys.path.append(os.environ["BEEFROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import (get_exe, parse_samples)

# before we do anything, check if the executable is there
exe = get_exe("approximate_discrete_profiling")

samples = snakemake.input.get("sample", None)
more_samples = snakemake.params.get("sample", None)
wd   = snakemake.params.get("work_dir", "$RF")
wn   = snakemake.params.get("workspace_name", "w")
dsn  = snakemake.params.get("dataset_name", "Dred")
poin = snakemake.params.get("poi_name", "N")
mn   = snakemake.params.get("model_name", "model")
poit = snakemake.params.get("poi_title", "N")
nlln = snakemake.params.get("nll_name", None)
vb   = snakemake.params.get("verbosity", None)
tee  = snakemake.params.get("tee", ">")

out = snakemake.output

# make sure that all inputs are what we expect them to be
assert samples is not None or more_samples is not None, "\"input.sample\" or non-workflow input from \"params.sample\" is missing"
ifn = parse_samples(samples,more_samples)

assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(wn,str), "\"params.workspace_name\" is expected to be a string"
assert isinstance(dsn,str), "\"params.dataset_name\" is expected to be a string"
assert isinstance(poin,str), "\"params.poi_name\" is expected to be a string"
assert isinstance(mn,str), "\"params.model_name\" is expected to be a string"
assert isinstance(poit,str), "\"params.poi_title\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"

vb = "-v"+str(vb) if vb is not None else ""
no = "-n "+str(nlln) if nlln is not None else ""

# strip work_dir from output file
of = out[0].replace(os.path.expandvars(wd),"")
of = of[1:] if of.startswith("/") else of

shell("{exe} -d {wd} -i '{ifn}' -o {of} -r {dsn} -w {wn} -j '{poit}' -m {mn} -p {poin} {no} {vb} {tee} {snakemake.log} 2>&1")