"""
Snakemake wrapper for generate_toys.
For script documentation see
https://gitlab.cern.ch/mstahl/beef/-/blob/update/doc/generate_toys.md

The wrapper consumes inputs from the "input.sample" node.
This means that input files have to be created in the workflow; usually by a preceeding fit.

The "_idx.root" suffix that is appended automatically in generate_toys is stripped in this wrapper,
such that output files can be checked by snakemake, and the -o command line option is assembled correctly.

The wrapper needs:
- input.file (string): an input file containing a RooWorkspace with RooAbsPdf. Either `file` or the first input is consumed.
- output (list): the list of output files generated. Snakemake does not support functions as output files, but we can use multiext and unpacking argument lists while retaining wildcards like this
                 output = multiext(work_dir+"/toys/{channel}{period}_sel{selection}_fit{fittype}_",*[f"{i}.root" for i in range(0,40)])
- params.config (str): config file. This can be standalone, but for convenience it is also possible to use the one from the preceeding fit.
- params.ntoys (int): number of toys to generate. recommended to parse from output file list (ntoys = lambda wildcards, output : int(output[-1].split("_")[-1].strip(".root"))+1)
Optional arguments:
- params.work_dir (str): working directory, default "$RF".
- params.tee (str): for logging, default ">".
- params.workspace_name (str): RooWorkspace name in input file, default "w".
- params.model_name (str): RooAbsPdf name in input file, default "model".
- params.verbosity (castable to str)
- params.<anything> (str or dict): used to assemble joker string to replace stuff in config file.
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
sys.path.append(os.environ["BEEFROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import (get_exe, parse_joker)

# before we do anything, check if the executable is there
exe = get_exe("generate_toys")

file = snakemake.input.get("file",snakemake.input[0])
cfg  = snakemake.params.get("config", None)
wd   = snakemake.params.get("work_dir", "$RF")
wn   = snakemake.params.get("workspace_name", "w")
mn   = snakemake.params.get("model_name", "model")
n    = snakemake.params.get("ntoys", None)
vb   = snakemake.params.get("verbosity", None)
tee  = snakemake.params.get("tee", ">")
out  = snakemake.output

# make sure that all inputs are what we expect them to be
assert file is not None and isinstance(file,str), "\"input\" is missing or not a string"
assert n is not None, "\"params.ntoys\" has to be provided"
assert cfg is not None and isinstance(cfg,str), "\"params.config\" has to be provided and needs to be a string"
assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(wn,str), "\"params.workspace_name\" is expected to be a string"
assert isinstance(mn,str), "\"params.model_name\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"


if vb is not None: vb = "-v"+str(vb)
else : vb = ""

# parse joker arguments
joker = parse_joker(snakemake.params,["config","work_dir","workspace_name","model_name","tee","file","verbosity"])

# strip work_dir from output file
of = out[0].replace(os.path.expandvars(wd),"")
of = of[1:] if of.startswith("/") else of
of = "_".join(of.split("_")[:-1])

shell("{exe} -c {cfg} -d {wd} -i {file} -o {of} -m {mn} -n {n} -w {wn} {joker} {vb} {tee} {snakemake.log} 2>&1")