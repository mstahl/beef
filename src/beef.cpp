#include "RooDataHist.h"
#include "RooFormulaVar.h"
#include "TStopwatch.h"
//boost
#include <boost/property_tree/json_parser.hpp>
//local
#include <beef_utils.h>
#include <plotter.h>

#ifndef BEEF_GIT_HASH
#define BEEF_GIT_HASH " "
#endif

namespace pt = boost::property_tree;

//suppress RooBanner https://root.cern.ch/doc/master/RooBanner_8cxx_source.html
int doBanner() { return 0; }

int main(int argc, char** argv) {
  //////////////////////////////////////////////////////////
  ///  parse command-line options and get first objects  ///
  //////////////////////////////////////////////////////////
  TStopwatch clock;
  clock.Start();
  auto options = IOjuggler::parse_options(argc, argv, "c:d:hi:j:o:r:v:w:", "nonoptions: cutstring", 0);
  MessageService msgsvc("beef", static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current beef git hash: " + static_cast<std::string>(BEEF_GIT_HASH));
  //open file, check for absolute path and a path with prepended workdir
  const auto wd = options.get<std::string>("workdir");
  const auto ofn = options.get<std::string>("outfilename");
  const auto wsn = options.get<std::string>("wsname");
  const auto ifn = options.get<std::string>("infilename");
  // get the workspace; if there are multiple input files, get the full workspace of the first input, and add pdfs only from the other files.
  RooWorkspace* w = nullptr;
  if(ifn.find(";") != std::string::npos || ifn.find("@") != std::string::npos){
    const auto input_files = IOjuggler::parse_filenames(ifn, wd, msgsvc);
    w = IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(input_files[0], wd), wsn);
    for(unsigned j = 1; j < input_files.size(); j++){
      if(input_files[j].find(":") != std::string::npos){
        auto [tmp_ifn, tmp_wsn] = IOjuggler::split_at_last_occurence(input_files[j]);
        w->import(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(tmp_ifn, wd), tmp_wsn)->allPdfs());
      }
      else w->import(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(input_files[j], wd), wsn)->allPdfs());
    }
  }
  else
    w = IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(ifn, wd), wsn);
  //parse nonoptions. they are used for a (single) cutstring and/or for constraining/fixing paramters from earlier fitresults
  std::string selcmdl = "";
  std::vector<TString> constraints;
  for (const auto& nonopt : *options.get_child_optional("extraargs")) {
    TString extraoptStr = nonopt.second.data();
    if (extraoptStr.Contains(":"))
      constraints.push_back(extraoptStr);
    else
      selcmdl = extraoptStr;
  }

  ///////////////////////////////
  ///  runtime configuration  ///
  ///////////////////////////////
  pt::ptree configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  IOjuggler::replace_from_cmdline(configtree, options.get<std::string>("joker"));
  //append and replace stuff in ptree
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if (msgsvc.GetMsgLvl() == MSG_LVL::DEBUG) IOjuggler::print_ptree(configtree);
  IOjuggler::SetRootVerbosity(configtree.get_optional<int>("suppress_RootInfo"));
  if(const auto iconf = configtree.get_child_optional("IntegratorConfig")) beef_utils::configure_integrator(std::move(*iconf));

  /////////////////////
  ///  build model  ///
  /////////////////////
  if (!configtree.get_child_optional("model")) throw std::runtime_error("child \"model\" not found in config file");
  msgsvc.infomsg("Model: ");
  for (const auto& v : configtree.get_child("model")) {
    std::string component = v.second.data();
    msgsvc.infomsg("      " + component);
    w->factory(component.c_str());
  }
  const auto modelname = configtree.get_optional<std::string>("pdf");
  if (!modelname) throw std::runtime_error("\"pdf\" not found in config file");
  std::string model_name_for_fit = *modelname;
  const auto sim_node = configtree.get_child_optional("simultaneous");
  if (sim_node) beef_utils::build_sim_model(w, configtree, model_name_for_fit);
  auto PDF = IOjuggler::get_wsobj<RooAbsPdf>(w, model_name_for_fit);
  //helper variables
  if(const auto helper_variables = configtree.get_child_optional("auxiliaries"))
    for (const auto& aux : *helper_variables)
      w->factory(aux.second.get_value<std::string>().data());

  ////////////////////////////
  ///  prepare dataset(s)  ///
  ////////////////////////////
  auto ds_name_for_fit = configtree.get("dsName", "Dred");
  //cuts can have 2 sources: config file and command line
  auto selection = configtree.get<std::string>("selection", "");
  bool parse_sim_cut_from_nonopt = false;
  if (sim_node)
    if (!((*sim_node).front().first.find("SplitParam") == std::string::npos))
      parse_sim_cut_from_nonopt = true;
  //only add to selection if no explicit cuts are defined in simultaneous node
  if (!parse_sim_cut_from_nonopt && !selcmdl.empty()) selection += selcmdl;
  //get dataset with potential cuts and weights
  auto weight = configtree.get<std::string>("weight", "");
  // get dataset
  auto dsFull = IOjuggler::get_wsobj<RooDataSet>(w, options.get<std::string>("dsname"));
  msgsvc.infomsg(TString::Format("%.0f candidates in dataset", dsFull->sumEntries()));
  if (!selection.empty()) msgsvc.infomsg("Selection: " + selection);
  if (!weight.empty()) {
    if (!configtree.get_optional<bool>("fitoptions.SumW2Error") && !configtree.get_optional<bool>("fitoptions.AsymptoticError")) {
      msgsvc.warningmsg("You are working with a weighted dataset, but did not make use of the SumW2Error or AsymptoticError option.");
      msgsvc.warningmsg("The uncertainties returned by the fit will only be correct if your weights are properly normalised!");
    }
    if (auto owfs = configtree.get_optional<std::string>("weight_function")) {
      auto weight_var = IOjuggler::get_wsobj<RooRealVar>(w, weight);
      RooFormulaVar wFunc("wfunc", "event weight", (*owfs).data(), *weight_var);
      auto addedwf = static_cast<RooRealVar*>(dsFull->addColumn(wFunc));
      weight = addedwf->GetName();
    }
    msgsvc.infomsg("Applying weight "+weight);
  }
  RooDataSet ds(ds_name_for_fit.data(), ds_name_for_fit.data(), dsFull, *dsFull->get(), selection.data(), weight.empty() ? 0 : weight.data());
  msgsvc.infomsg(TString::Format("%.0f candidates selected", ds.sumEntries()));
  w->import(ds);
  //prepare dataset for simultaneous fit
  if (sim_node) beef_utils::label_dataset(w, PDF, configtree, ds_name_for_fit, parse_sim_cut_from_nonopt, std::move(selcmdl));

  ////////////////////////////////////////////////
  ///  prepare fit, constraints, observables   ///
  ////////////////////////////////////////////////
  RooArgSet parameters_for_minos;
  RooArgSet* constrainFuncs = nullptr;
  // set all fit options
  auto args = beef_utils::set_fit_options(w,configtree,parameters_for_minos);
  // make constraints
  if (!constraints.empty() || configtree.get_child_optional("constrainParams")) {
    constrainFuncs = static_cast<RooArgSet*>(beef_utils::make_constraints(w, configtree, constraints, wd, wsn).Clone());
    args.push_back(new RooCmdArg(RooFit::ExternalConstraints(*constrainFuncs)));
  }

  RooLinkedList llist;
  for (auto arg : args) {
    //the next line forbids usage of RooCmdArgs as r-value,
    //also taking the adress of a reference and casting to TObject* can't be handled by RooFit, thus the pointers
    arg->setProcessRecArgs(true, false);
    llist.Add(arg);
  }
  auto DATA = IOjuggler::get_wsobj<RooDataSet>(w, ds_name_for_fit);
  if(msgsvc.GetMsgLvl() > MSG_LVL::INFO) DATA->Print();

  const bool as_hist = configtree.get("fitoptions.FitAsHist", 0);
  // the main reason to outsource this is the range parsing of observables (which is a bit messy)
  const auto observables = beef_utils::get_observables(w,DATA,configtree,as_hist);
  if(msgsvc.GetMsgLvl() > MSG_LVL::INFO){
    auto oit = observables.createIterator();
    while (const auto obs = oit->Next()) obs->Print();
  }
  /////////////////////
  ///  execute fit  ///
  /////////////////////
  RooFitResult* fr{nullptr};
  if (as_hist) {
    msgsvc.infomsg("Converting dataset to histogram and performing a binned fit");
    RooDataHist hist("hist", "hist", observables, *DATA);
    fr = PDF->fitTo(hist, llist);
  } else fr = PDF->fitTo(*DATA, llist);

  for (auto arg : args)
    delete arg;
  args.clear();
  llist.Clear();
  parameters_for_minos.Clear();

  /////////////////////////
  ///  process results  ///
  /////////////////////////
  int status = 0;
  const auto edm = fr->edm();
  const auto fs = fr->status();
  const auto cq = fr->covQual();
  if (edm > 0.001)
    msgsvc.warningmsg("fit has large estimated distance to minimum (edm): " + std::to_string(edm));
  if (fs != 0) {
    status += fs;
    msgsvc.warningmsg("fit has bad fit status: " + std::to_string(fs));
  }
  if (cq != 3) {
    msgsvc.warningmsg("fit has bad covariance matrix: " + std::to_string(cq));
    if (configtree.get("no_bad_cov", 1))
      status += cq * 100;
  }
  if (status != 0)
    msgsvc.warningmsg("Bad fit status: " + std::to_string(status));
  //print fitresults to stdout
  if (configtree.get("printFitResult", 0))
    beef_utils::print_fitresult(*fr, constrainFuncs, configtree.get_optional<bool>("fitoptions.Offset") ? PDF->createNLL(*DATA)->getVal() : 0.,
                                  configtree.get<double>("npars_scaling_for_nll_correction",0.5) * PDF->getParameters(*DATA)->size(), msgsvc);
  if (configtree.get("printCorrelation", 0)) beef_utils::print_root_correlation_matrix(fr->correlationMatrix(), msgsvc);

  //////////////////////////////////////////////
  ///  output to disk : plots and root file  ///
  //////////////////////////////////////////////
  auto call_plotter = [&w,&DATA,&PDF,&ofn,&wd] (const pt::ptree&& plotnode) -> void {
    for (const auto& plot : plotnode)
      if (plot.second.get_optional<std::string>("var"))  //there could be REPLACE/APPEND nodes
        plotter(w, *DATA, PDF, ofn, wd, plot.second);
  };
  if (const auto plotnodeopt = configtree.get_child_optional("plots")) call_plotter(std::move(*plotnodeopt));
  else if (!plotnodeopt && sim_node){
    auto titer = IOjuggler::get_wsobj<RooCategory>(w, static_cast<RooSimultaneous*>(PDF)->indexCat().GetName())->typeIterator();
    while (const auto vcat = titer->Next())
      if (const auto& simpnode = configtree.get_child_optional("plots_" + static_cast<std::string>(vcat->GetName()))) call_plotter(std::move(*simpnode));
  }

  // prepare the root file and put in there what we can already
  const auto outfilename = wd + "/" + ofn;
  IOjuggler::dir_exists(outfilename, msgsvc);
  //create a new workspace to avoid seg-faults arising from cached pdfs like RooFFTConvPdf when trying to access the old workspace in the new file
  auto outfile = TFile::Open(outfilename.data(), "RECREATE");
  RooWorkspace ws("w", true);
  ws.import(*fr); // we don't wan't Splot to screw up our nice fitresult, so we save it already now
  ws.import(*PDF);
  if(constrainFuncs != nullptr) ws.import(*constrainFuncs);
  if(const auto helper_variables = configtree.get_child_optional("auxiliaries"))
    for (const auto& aux : *helper_variables)
      ws.import(*IOjuggler::get_wsobj<RooFormulaVar>(w,aux.first));

  /////////////////////////////////////////////
  ///  calculate sweights, close and clean  ///
  /////////////////////////////////////////////
  if(configtree.get_child_optional("extended")) beef_utils::get_sweights(DATA, PDF, fr, configtree);
  if(configtree.get("save_dataset",1)) ws.import(*DATA);  // if there are sweights, they are saved by importing data
  if(const auto create_nll = configtree.get_optional<std::string>("minNLL"); create_nll){
    RooRealVar minnll{(*create_nll).data(),(*create_nll).data(),PDF->createNLL(*DATA)->getVal()};
    ws.import(minnll);
  }
  if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) ws.Print();
  ws.Write();
  outfile->Write();

  /////////////////////////////////////////////
  ///  output to disk : info and tex files  ///
  /////////////////////////////////////////////
  //make ptfr global as the efficiency calculation later might use it. could be solved by dumping everything into the workspace as well, but meh...
  pt::ptree ptfr;
  if(const auto qconf = configtree.get_child_optional("getQuantiles"))      beef_utils::get_quantiles(wd, ws, std::move(*qconf), outfilename, ptfr);
  if(const auto rcut  = configtree.get_child_optional("getEntriesInRange")) beef_utils::get_entries_in_range(wd, std::move(*rcut), outfilename, ptfr, DATA);
  if(const auto iconf = configtree.get_child_optional("getIntegral"))       beef_utils::get_integral(wd, ws, std::move(*iconf), outfilename, ptfr);
  if(configtree.get_child_optional("saveFitResultsAsInfo"))                 beef_utils::save_fitresult_as_info(ws, fr, configtree, ptfr);
  if(configtree.get_child_optional("saveFitResultsAsTeX"))                  beef_utils::save_fitresult_as_tex(ws, fr, configtree, outfilename);
  if(const auto effconf = configtree.get_child_optional("efficiency"))      beef_utils::calculate_efficiencies(ws, std::move(*effconf), ptfr);
  if(!ptfr.empty()){
    pt::info_parser::write_info(boost::algorithm::replace_all_copy(outfilename, ".root", ".info"), ptfr);
    msgsvc.infomsg("Wrote info file with results to "+boost::algorithm::replace_all_copy(outfilename, ".root", ".info"));
    if(configtree.get("write_json",0)){
      pt::json_parser::write_json(boost::algorithm::replace_all_copy(outfilename, ".root", ".json"), ptfr);
      msgsvc.infomsg("Wrote json file with results to "+boost::algorithm::replace_all_copy(outfilename, ".root", ".json"));
    }
  }

#if ROOT_VERSION_CODE < ROOT_VERSION(6, 19, 0)
  //cleanup
  outfile->Delete("*");
  outfile->Close("R");
  delete w;
  delete fr;
  delete outfile;
#endif
  msgsvc.infomsg("Wrote root file with results to "+outfilename);
  clock.Stop();
  if (msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();

  return 0;
}
