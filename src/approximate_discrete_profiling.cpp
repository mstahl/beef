/**
  @date:   2022-08-15
  @author: mstahl
  @brief:  Implements an approximation of the NLLs used for the discrete profiling method https://inspirehep.net/literature/1312971 .
  @details: It's assumed that the NLL can be approximated with a (bifurcated) 2nd order polynomial (estimator is distributed as (bifurcated) Gaussian in asymptotic limit).
            The reason for doing this is that we'd like to use profile likelihoods instead of simple ones to account for nuisance parameters;
            and calculating reasonably binned profile likelihoods is painfully slow as each bin requires a new minimization/fit.
            The trick is to use the NLL at best fit value and MINOS uncertainties to get the polynomial approximation to the profile likelihood
            (the reported upper and lower MINOS errors correspond to a delta log likelihood of 0.5).
**/
//boost
#include <boost/property_tree/json_parser.hpp>
//root
#include "RooAbsPdf.h"
#include "RooGaussian.h"
#include "RooRealVar.h"
#include "TAxis.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
//local
#include "lhcbStyle.C"
#include "CustomPalettes.h"
#include "IOjuggler.h"

#ifndef BEEF_GIT_HASH
#define BEEF_GIT_HASH " "
#endif

//suppress RooBanner https://root.cern.ch/doc/master/RooBanner_8cxx_source.html
int doBanner() { return 0; }

int main(int argc, char** argv) {
  RooMsgService::instance().setGlobalKillBelow(static_cast<RooFit::MsgLevel>(4));// 4 ERROR
  TStopwatch clock;
  clock.Start();
  auto options = IOjuggler::parse_options(argc, argv, "d:hi:j:m:n:o:p:r:v:w:");
  MessageService msgsvc("approximate_discrete_profiling", static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current beef git hash: " + static_cast<std::string>(BEEF_GIT_HASH));
  const auto wd   = options.get<std::string>("workdir");
  const auto ofn  = options.get<std::string>("outfilename");
  const auto wsn  = options.get<std::string>("wsname");
  const auto dsn  = options.get<std::string>("dsname");
  const auto ifn  = options.get<std::string>("infilename");
  const auto modn = options.get<std::string>("weightfile");
  const auto poin = options.get<std::string>("prefix");
  const auto poit = options.get<std::string>("joker","N");
  const auto nll_name = options.get<std::string>("n");
  msgsvc.debugmsg(nll_name);

  // https://inspirehep.net/literature/1312971 Sec. 4.5., note that we use -log(L), not -2*log(L)
  constexpr auto npar_scaling = 0.5;

  // open input files and put data, pdf, parameters of interest and nll into vectors
  // plot ranges (y is NLL, x is poi)
  auto ylo = std::numeric_limits<double>::infinity();
  auto xhathat = 0., dxhathat_lo = 0., dxhathat_hi = 0.;
  std::vector<TString> parabolas;
  std::vector<double> best_fitvals, as_lo, as_hi, min_nlls, corrections;

  auto get_approximation = [&] (const auto&& iws) -> void {
    const auto data = iws->data(dsn.data());
    const auto pdf = iws->pdf(modn.data());
    auto var = iws->var(poin.data());
    const auto poi_best_fitval = var->getVal();
    const auto poi_best_fiterr_lo = var->getErrorLo();
    const auto poi_best_fiterr_hi = var->getErrorHi();
    msgsvc.infomsg(TString::Format("Best fit value %.0f %.0f +%.0f",poi_best_fitval,poi_best_fiterr_lo,poi_best_fiterr_hi));
    // likelihood correction for number of paramters and pulls from constraints
    auto nll_correction = npar_scaling * pdf->getParameters(*data)->size();
    for(const auto& abs_arg : iws->allPdfs()){
      if(auto pdf_name = static_cast<std::string>(abs_arg->GetName()); pdf_name.find("constr_") == 0){
        const auto gc = static_cast<RooGaussian*>(abs_arg);
        const auto constr_val = static_cast<RooRealVar*>(gc->getVariables()->find(("mean_"+pdf_name.substr(7)).data()))->getVal();
        const auto constr_err = static_cast<RooRealVar*>(gc->getVariables()->find(("width_"+pdf_name.substr(7)).data()))->getVal();
        const auto pull = (static_cast<RooAbsReal*>(iws->obj(pdf_name.substr(7).data()))->getVal() - constr_val) / constr_err;
        nll_correction += 0.5*abs(pull);
      }
    }
    // after fitting, all RooRealVars are snapshotted at the best fit. So NLL is minimal when we call it; add the number of (all) paramters of the pdf
    // (usually that's the case, but not always... it's possible to store the nll as RooRealVar in beef, and we might call it here)
    const auto tmp_min_nll = nll_name == "1000" ? pdf->createNLL(*data)->getVal() : iws->var(nll_name.data())->getVal() + nll_correction; // https://inspirehep.net/literature/1312971 Sec. 4.5
    msgsvc.debugmsg(TString::Format("Corrected min NLL %.3f  correction %.3f",tmp_min_nll,nll_correction));
    if (tmp_min_nll < ylo){
      ylo = tmp_min_nll;
      xhathat = poi_best_fitval;
      dxhathat_lo = abs(poi_best_fiterr_lo);
      dxhathat_hi = poi_best_fiterr_hi;
    }

    // get approximating parabola
    // nll_at_one_sigma = a * ||poi_at_one_sigma - poi_best_fitval||^2 + tmp_min_nll
    // ==> a = (nll_at_one_sigma - tmp_min_nll) / ||poi_at_one_sigma - poi_best_fitval||^2
    double a_lo = 0.5 / pow(poi_best_fiterr_lo,2);
    double a_hi = 0.5 / pow(poi_best_fiterr_hi,2);
    best_fitvals.push_back(poi_best_fitval);
    as_lo.push_back(a_lo);
    as_hi.push_back(a_hi);
    min_nlls.push_back(tmp_min_nll);
    corrections.push_back(nll_correction);
  };
  // loop input files and find plotting range
  auto n_inputs = 1u;

  if(ifn.find(";") != std::string::npos || ifn.find("@") != std::string::npos){
    const auto input_files = IOjuggler::parse_filenames(ifn, wd, msgsvc);
    n_inputs = input_files.size();
    for(unsigned j = 0; j < input_files.size(); j++){
      if(input_files[j].find(":") != std::string::npos){
        auto [tmp_ifn, tmp_wsn] = IOjuggler::split_at_last_occurence(input_files[j]);
        get_approximation(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(tmp_ifn, wd), tmp_wsn));
      }
      else
        get_approximation(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(input_files[j], wd), wsn));
    }
  }
  else
    get_approximation(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(ifn, wd), wsn));

  // get the envelope
  // need to assume that the best fit model returned the best global NLL.
  // then: find the point where a tangent of the "outermost" NLL is equal to the tangent of the envelope.
  // (system of 2 equations: functions and first derivatives of local NLL and envelope are equal in point x_1,y_1)
  auto x_dist_env_onesigma_lo = dxhathat_lo, x_dist_env_onesigma_hi = dxhathat_hi, a_env_lo = 0., a_env_hi = 0.;
  for(auto i=0u; i<best_fitvals.size(); i++) {
    min_nlls[i] -= ylo;
    parabolas.emplace_back(TString::Format("(x < %.6e ? %.6e : %.6e) * pow(x - %.6e,2) + %.6e",best_fitvals[i],as_lo[i],as_hi[i],best_fitvals[i],min_nlls[i]));
    if(min_nlls[i]==0){
      // set a_env to best fit in case there's no real envelope yet
      if(a_env_lo == 0.) a_env_lo = as_lo[i];
      if(a_env_hi == 0.) a_env_hi = as_hi[i];
      continue;
    }
    auto x_1_lo = best_fitvals[i]+min_nlls[i]/(as_lo[i]*(best_fitvals[i]-xhathat));
    auto y_1_lo = as_lo[i]*pow(x_1_lo-best_fitvals[i],2)+min_nlls[i];
    auto tmp_a_env_lo = y_1_lo/pow(x_1_lo-xhathat,2);
    auto tmp_xdist_lo = 1./std::sqrt(2*tmp_a_env_lo);
    msgsvc.debugmsg(TString::Format("Fit %c: xhat %.1f  yhat %.4f  xhathat %.1f  ahat %.6e  x1 %.1f   y1 %.4f  a_env %.6e  xdist_lo %.2f",
                                    'A'+i, best_fitvals[i], min_nlls[i], xhathat, as_lo[i], x_1_lo, y_1_lo, tmp_a_env_lo, tmp_xdist_lo));
    if(tmp_xdist_lo > x_dist_env_onesigma_lo){
      x_dist_env_onesigma_lo = tmp_xdist_lo;
      a_env_lo = tmp_a_env_lo;
    }

    auto x_1_hi = best_fitvals[i]+min_nlls[i]/(as_hi[i]*(best_fitvals[i]-xhathat));
    auto y_1_hi = as_hi[i]*pow(x_1_hi-best_fitvals[i],2)+min_nlls[i];
    auto tmp_a_env_hi = y_1_hi/pow(x_1_hi-xhathat,2);
    auto tmp_xdist_hi = 1./std::sqrt(2*tmp_a_env_hi);
    msgsvc.debugmsg(TString::Format("Fit %c: xhat %.1f  yhat %.4f  xhathat %.1f  ahat %.6e  x1 %.1f  y1 %.4f  a_env %.6e  xdist_hi %.2f",
                                    'A'+i, best_fitvals[i], min_nlls[i], xhathat, as_hi[i], x_1_hi, y_1_hi, tmp_a_env_hi, tmp_xdist_hi));

    if(tmp_xdist_hi > x_dist_env_onesigma_hi){
      x_dist_env_onesigma_hi = tmp_xdist_hi;
      a_env_hi = tmp_a_env_hi;
    }
  }
  // for the plotting range
  const auto xlo = xhathat - 2/std::sqrt(a_env_lo);
  const auto xhi = xhathat + 2/std::sqrt(a_env_hi);

  // print and write ouput
  const auto outfilename = wd + "/" + ofn;
  IOjuggler::dir_exists(outfilename, msgsvc);
  const auto unc_nominal_lo = 1./std::sqrt(2*as_lo[0]), unc_nominal_hi = 1./std::sqrt(2*as_hi[0]);
  const auto unc_profile_lo = x_dist_env_onesigma_lo > unc_nominal_lo ? std::sqrt(pow(x_dist_env_onesigma_lo,2)-pow(unc_nominal_lo,2)) : 0.;
  const auto unc_profile_hi = x_dist_env_onesigma_hi > unc_nominal_hi ? std::sqrt(pow(x_dist_env_onesigma_hi,2)-pow(unc_nominal_hi,2)) : 0.;
  //msgsvc.infomsg(TString::Format("Result %s = %.0f -%.0f +%.0f (stat) -%.0f +%.0f (profile)",poin.data(),xhathat,dxhathat_lo,dxhathat_hi,unc_profile_lo,unc_profile_hi));
  msgsvc.infomsg(TString::Format("Result %s = %.0f -%.0f +%.0f (stat) -%.0f +%.0f (profile)",poin.data(),best_fitvals[0],1./std::sqrt(2*as_lo[0]),1./std::sqrt(2*as_hi[0]),unc_profile_lo,unc_profile_hi));

  boost::property_tree::ptree ptfr;
  ptfr.add("unc_profile_lo",unc_profile_lo);
  ptfr.add("unc_profile_hi",unc_profile_hi);
  ptfr.add("best_fit",xhathat);
  ptfr.add("unc_best_fit_lo",dxhathat_lo);
  ptfr.add("unc_best_fit_hi",dxhathat_hi);
  ptfr.add("nominal_fit",best_fitvals[0]);
  ptfr.add("unc_nominal_fit_lo",1./std::sqrt(2*as_lo[0]));
  ptfr.add("unc_nominal_fit_hi",1./std::sqrt(2*as_hi[0]));
  boost::property_tree::json_parser::write_json(outfilename, ptfr);
  msgsvc.infomsg("Wrote json file with results to "+outfilename);

  // make a plot
  // set basic plot style
  lhcbStyle(static_cast<int>(msgsvc.GetMsgLvl()));
  set_CustomPalette(118);
  TCanvas c1("c1","",2724,1280);
  c1.SetLeftMargin(0.1);
  c1.SetBottomMargin(0.18);
  c1.SetRightMargin(0.25);
  c1.SetTopMargin(0.025);
  std::vector<TF1> approx_nlls(n_inputs);
  const auto colors_inc = gStyle->GetNumberOfColors()/(n_inputs+1);
  for(unsigned i = 0; i < n_inputs; i++){
    msgsvc.debugmsg(parabolas[i]);
    approx_nlls[i] = TF1(TString::Format("%c",'A'+i).Data(),parabolas[i].Data(),xlo,xhi);
    if(i==0){
      approx_nlls[i].GetXaxis()->SetTitle(poit.data());
      approx_nlls[i].GetYaxis()->SetTitle("#Delta log(#lambda)");
      approx_nlls[i].GetYaxis()->SetTitleOffset(0.65);
      approx_nlls[i].GetYaxis()->SetRangeUser(-.05,3.95);
    }
    // have to do it like this as the legend doesn't pick up the color for TF1 when drawn with "plc" option...
    approx_nlls[i].SetLineColor(gStyle->GetColorPalette((i+1)*colors_inc));
    if(min_nlls[i] > 4.)approx_nlls[i].SetLineStyle(2);
    approx_nlls[i].Draw((std::string{"c"}+static_cast<std::string>(i>0 ? "same" : "")).data());
  }
  auto envelope = TF1("envelope",TString::Format("(x < %.6e ? %.6e : %.6e) * pow(x - %.6e,2)",xhathat,a_env_lo,a_env_hi,xhathat).Data(),xlo,xhi);
  msgsvc.debugmsg(TString::Format("(x < %.6e ? %.6e : %.6e) * pow(x - %.6e,2)",xhathat,a_env_lo,a_env_hi,xhathat));
  envelope.SetLineColor(1);
  envelope.Draw("csame");

  TLegend leg(0.755,0.995-(n_inputs+2)*0.09,0.995,0.995);
  leg.SetTextSize(0.06);
  for(unsigned i = 0; i < n_inputs; i++)
   leg.AddEntry(&approx_nlls[i],TString::Format("Fit %c",'A'+i).Data(),"l");
  leg.AddEntry("envelope","Envelope","l");
  leg.Draw();
  TLatex ltx;
  ltx.SetTextSize(0.06);
  ltx.DrawLatexNDC(0.18,0.25,"LHCb");

  approx_nlls[0].Draw("axissame");
  c1.SaveAs((boost::replace_all_copy(outfilename,".json",".pdf")).data());
  c1.SaveAs((boost::replace_all_copy(outfilename,".json",".tex")).data());

  clock.Stop();
  if (msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();
  return 0;
}
