#include "RooAbsPdf.h"
#include "RooRealVar.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TStopwatch.h"
//local
#include "lhcbStyle.C"
#include "IOjuggler.h"

#ifndef BEEF_GIT_HASH
#define BEEF_GIT_HASH " "
#endif

//suppress RooBanner https://root.cern.ch/doc/master/RooBanner_8cxx_source.html
int doBanner() { return 0; }

int main(int argc, char** argv) {
  RooMsgService::instance().setGlobalKillBelow(static_cast<RooFit::MsgLevel>(3));// 3 WARNING
  TStopwatch clock;
  clock.Start();
  auto options = IOjuggler::parse_options(argc, argv, "d:hi:j:m:o:p:r:v:w:");
  MessageService msgsvc("discrete_profiling", static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current beef git hash: " + static_cast<std::string>(BEEF_GIT_HASH));
  const auto wd   = options.get<std::string>("workdir");
  const auto ofn  = options.get<std::string>("outfilename");
  const auto wsn  = options.get<std::string>("wsname");
  const auto dsn  = options.get<std::string>("dsname");
  const auto ifn  = options.get<std::string>("infilename");
  const auto modn = options.get<std::string>("weightfile");
  const auto poin = options.get<std::string>("prefix");
  const auto poit = options.get<std::string>("joker","N");

  // https://inspirehep.net/literature/1312971 Sec. 4.5. here, we use -log(L), not -2*log(L)
  constexpr auto npar_scaling = 0.5;

  // open input files and put data, pdf, parameters of interest and nll into vectors
  // plot ranges (y is NLL, x is poi)
  auto ylo = std::numeric_limits<double>::infinity(), yhi = -std::numeric_limits<double>::infinity();
  auto xlo = std::numeric_limits<double>::infinity(), xhi = -std::numeric_limits<double>::infinity();

  auto get_plotting_range = [/*&ws,*/ &dsn, &poin, &modn, &ylo, &yhi, &xlo, &xhi, npar_scaling, &msgsvc] (const auto&& iws) -> void {
    // this is a quick and dirty solution. if it takes too much time to e.g. `createNLL`,
    // the cleaner solution would be trying to keep everything (ws, nll, etc.) in scope so it's only created once.
    const auto data = iws->data(dsn.data());
    const auto pdf = iws->pdf(modn.data());
    const auto nll = pdf->createNLL(*data);
    auto var = iws->var(poin.data());
    const auto npars = pdf->getParameters(*data)->size();

    // after fitting, all RooRealVars are snapshotted at the best fit. So NLL is minimal when we call it; add the number of (all) paramters of the pdf
    const auto tmp_min_nll = nll->getVal() + npar_scaling*npars; // https://inspirehep.net/literature/1312971 Sec. 4.5
    msgsvc.infomsg("Current min NLL "+std::to_string(tmp_min_nll));
    if (tmp_min_nll < ylo) ylo = tmp_min_nll;
    // 1 sigma should be delta NLL of 0.5; let's plot up to 3 sigma
    // what we'd like to do is plotting profile likelihoods and use MINOS uncertainties;
    // for plotting that's fine, for the method itself, we can scale up later
    const auto poi_best_fitval = var->getVal();
    const auto poi_best_fiterr = var->getError();

    const auto tmp_xlo = poi_best_fitval - 3 * poi_best_fiterr;
    if (tmp_xlo < xlo) xlo = tmp_xlo;
    const auto tmp_xhi = poi_best_fitval + 3 * poi_best_fiterr;
    if (tmp_xhi > xhi) xhi = tmp_xhi;
    var->setVal(poi_best_fitval - 3 * poi_best_fiterr);
    auto tmp_max_nll = nll->getVal() + npar_scaling*npars;
    if (tmp_max_nll > yhi) yhi = tmp_max_nll;
    var->setVal(poi_best_fitval + 3 * poi_best_fiterr);
    msgsvc.debugmsg("NLL at -3 sigma "+std::to_string(tmp_max_nll));
    tmp_max_nll = nll->getVal() + npar_scaling*npars;
    if (tmp_max_nll > yhi) yhi = tmp_max_nll;
    msgsvc.debugmsg("NLL at +3 sigma "+std::to_string(tmp_max_nll));
    // reset var (nll needs to be re-created; might work without, not tried)
    var->setVal(poi_best_fitval);
    return;
  };
  // loop input files and find plotting range
  auto n_inputs = 1u;
  if(ifn.find(";") != std::string::npos || ifn.find("@") != std::string::npos){
    const auto input_files = IOjuggler::parse_filenames(ifn, wd, msgsvc);
    n_inputs = input_files.size();
    for(unsigned j = 0; j < input_files.size(); j++){
      if(input_files[j].find(":") != std::string::npos){
        auto [tmp_ifn, tmp_wsn] = IOjuggler::split_at_last_occurence(input_files[j]);
        get_plotting_range(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(tmp_ifn, wd), tmp_wsn));
      }
      else
        get_plotting_range(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(input_files[j], wd), wsn));
    }
  }
  else
    get_plotting_range(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(ifn, wd), wsn));

  // make a plot -- TH1 with c option 250 bins should be sufficient
  const auto outfilename = wd + "/" + ofn;
  IOjuggler::dir_exists(outfilename, msgsvc);
  auto outfile = TFile::Open(outfilename.data(), "RECREATE");
  // set basic plot style
  lhcbStyle(static_cast<int>(msgsvc.GetMsgLvl()));
  TCanvas c1("c1","",2048,1280);
  constexpr auto nbins = 250u;

  std::vector<TH1D> hists(n_inputs,{"",(";"+poit+";-log(L)").data(),nbins,xlo,xhi});

  auto make_plots = [&hists, &outfile, &dsn, &poin, &modn, ylo, yhi, npar_scaling, &msgsvc] (const auto&& iws, unsigned j) -> void {
    // this is a quick and dirty solution. if it takes too much time to e.g. `createNLL`,
    // the cleaner solution would be trying to keep everything (ws, nll, etc.) in scope so it's only created once.
    const auto data = iws->data(dsn.data());
    const auto pdf = iws->pdf(modn.data());
    auto var = iws->var(poin.data());
    const auto nll = pdf->createNLL(*data);
    const auto npars = pdf->getParameters(*data)->size();
    for(auto i = 1; i < nbins+1; i++){
      const auto poi_val = hists[j].GetBinCenter(i);
      var->setVal(poi_val);
      const auto tmp_nll = nll->getVal() + npar_scaling*npars;
      msgsvc.debugmsg("NLL for bin "+std::to_string(i)+" at "+std::to_string(poi_val)+" : "+std::to_string(tmp_nll));
      hists[j].SetBinContent(i, tmp_nll);
    }
    // actually, it's enough to set it for the first one only, and draw the rest with "same"
    hists[j].GetYaxis()->SetRangeUser(ylo-.2,yhi);
    outfile->cd();
    hists[j].Write();
    hists[j].Draw((std::string{"c"}+static_cast<std::string>(j>0 ? "same" : " ")).data());
    return;
  };
  // loop input files and make plot (also this could be nicer)
  if(ifn.find(";") != std::string::npos || ifn.find("@") != std::string::npos){
    const auto input_files = IOjuggler::parse_filenames(ifn, wd, msgsvc);
    for(unsigned j = 0; j < input_files.size(); j++){
      if(input_files[j].find(":") != std::string::npos){
        auto [tmp_ifn, tmp_wsn] = IOjuggler::split_at_last_occurence(input_files[j]);
        make_plots(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(tmp_ifn, wd), tmp_wsn),j);
      }
      else
        make_plots(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(input_files[j], wd), wsn),j);
    }
  }
  else
    make_plots(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(ifn, wd), wsn),0u);

  c1.SaveAs((boost::replace_all_copy(outfilename,".root",".pdf")).data());

  clock.Stop();
  if (msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();
  return 0;
}