#include "RooDataHist.h"
#include "RooMCStudy.h"
#include "TStopwatch.h"
//boost
#include <boost/property_tree/json_parser.hpp>
//local
#include <beef_utils.h>
#include <plotter.h>

#ifndef BEEF_GIT_HASH
#define BEEF_GIT_HASH " "
#endif

namespace pt = boost::property_tree;

//suppress RooBanner https://root.cern.ch/doc/master/RooBanner_8cxx_source.html
int doBanner() { return 0; }

int main(int argc, char** argv) {
  //////////////////////////////////////////////////////////
  ///  parse command-line options and get first objects  ///
  //////////////////////////////////////////////////////////
  TStopwatch clock;
  clock.Start();
  auto options = IOjuggler::parse_options(argc, argv, "c:d:hi:j:m:n:o:v:w:");
  MessageService msgsvc("beef", static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current beef git hash: " + static_cast<std::string>(BEEF_GIT_HASH));
  //open file, check for absolute path and a path with prepended workdir
  const auto wd = options.get<std::string>("workdir");
  const auto ofn = options.get<std::string>("outfilename");
  const auto wsn = options.get<std::string>("wsname");
  const auto ifn = options.get<std::string>("infilename");
  const auto ntoys = options.get<int>("n");

  ///////////////////////////////
  ///  runtime configuration  ///
  ///////////////////////////////
  pt::ptree configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  IOjuggler::replace_from_cmdline(configtree, options.get<std::string>("joker"));
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if (msgsvc.GetMsgLvl() == MSG_LVL::DEBUG) IOjuggler::print_ptree(configtree);
  IOjuggler::SetRootVerbosity(configtree.get_optional<int>("suppress_RootInfo"));

  //////////////////////////////////////////////////////
  ///  get model and observable for generating toys  ///
  //////////////////////////////////////////////////////
  const auto gen_ws = IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(ifn, wd), wsn);
  const auto gen_model = IOjuggler::get_wsobj<RooAbsPdf>(gen_ws,options.get<std::string>("weightfile"));

  RooArgSet observables;
  for (const auto& obs : configtree.get_child("observables"))
    observables.add(*IOjuggler::get_wsobj<RooRealVar>(gen_ws, obs.first));
  if(msgsvc.GetMsgLvl() > MSG_LVL::INFO){
    auto oit = observables.createIterator();
    while (const auto obs = oit->Next()) obs->Print();
    gen_model->Print();
  }

  ///////////////////////
  ///  generate toys  ///
  ///////////////////////
  // RooMCStudy won't take linked lists, so passing options has to be explicit
  RooMCStudy mcs(*gen_model,observables,
                 configtree.get_optional<bool>("VerboseToys") ? RooFit::Verbose() : RooCmdArg::none(),
                 configtree.get_optional<bool>("SilenceToys") ? RooFit::Silence() : RooCmdArg::none(),
                 configtree.get_optional<bool>("BinnedToys") ? RooFit::Binned(true) : RooCmdArg::none(),
                 configtree.get_optional<bool>("ExtendedToys") ? RooFit::Extended(true) : RooCmdArg::none());
  mcs.generate(ntoys,configtree.get("NEvtToys",0),true);
  //RooMsgService::instance().setGlobalKillBelow(RooFit::DEBUG);

  for(int i=0; i<ntoys; i++){
    // prepare the root file init workspace
    const std::string outfilename = TString::Format("%s/%s_%d.root",wd.data(),boost::algorithm::replace_all_copy(ofn,".root","").data(),i).Data();
    IOjuggler::dir_exists(outfilename, msgsvc);
    auto outfile = TFile::Open(outfilename.data(), "RECREATE");
    RooWorkspace ws(("toy_ws"+std::to_string(i)).data(), true);
    ws.import(observables);
    ws.import(*mcs.genData(i), RooFit::Rename("toy_ds"));
    if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) ws.Print();
    ws.Write();
    outfile->Write();
  }
  msgsvc.infomsg(TString::Format("Wrote root files with toys to %s/%s_<idx>.root",wd.data(),boost::algorithm::replace_all_copy(ofn,".root","").data()));
  clock.Stop();
  if (msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();

  return 0;
}
