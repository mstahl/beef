//STL
#include <cassert>
#include <iostream>
#include <string>
#include <vector>
//boost
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ptree.hpp>
//ROOT
#include "Math/ProbFuncMathCore.h"
#include "TAxis.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TStopwatch.h"
#include "TString.h"
#include "TStyle.h"
//RooFit
#include "RooAbsPdf.h"
#include "RooCategory.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooHist.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "RooSimultaneous.h"
#include "RooWorkspace.h"
//local
#include "CustomPalettes.h"
#include "IOjuggler.h"
#include "debug_helpers.h"
#include "lhcbStyle.C"

#ifndef BEEF_GIT_HASH
#define BEEF_GIT_HASH " "
#endif

// Use (void) to silent unused warnings.
#define assertm(exp, msg) assert(((void)msg, exp))

namespace pt = boost::property_tree;

//suppress RooBanner https://root.cern.ch/doc/master/RooBanner_8cxx_source.html
int doBanner() { return 0; }

int main(int argc, char** argv) {
  //////////////////////////////////////////////////////////
  ///  parse command-line options and get first objects  ///
  //////////////////////////////////////////////////////////
  TStopwatch clock;
  clock.Start();
  auto options = IOjuggler::parse_options(argc, argv, "c:d:hi:m:o:r:v:w:");
  MessageService msgsvc("beef", static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current beef git hash: " + static_cast<std::string>(BEEF_GIT_HASH));
  //open file, check for absolute path and a path with prepended workdir
  const auto wd = options.get<std::string>("workdir");
  const auto ofn = options.get<std::string>("outfilename");

  ///////////////////////////////
  ///  runtime configuration  ///
  ///////////////////////////////
  pt::ptree configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  //append and replace stuff in ptree
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if (msgsvc.GetMsgLvl() == MSG_LVL::DEBUG) IOjuggler::print_ptree(configtree);
  IOjuggler::SetRootVerbosity(configtree.get_optional<int>("suppress_RootInfo"));

  //terrible hack to make multiple plots in the same canvas without changing too much in the framework
  std::vector<std::string> infile_names, dataset_names, ws_names, model_names;
  boost::algorithm::split(infile_names, options.get<std::string>("infilename"), boost::algorithm::is_any_of(";"));
  boost::algorithm::split(dataset_names, options.get<std::string>("dsname"), boost::algorithm::is_any_of(";"));
  boost::algorithm::split(ws_names, options.get<std::string>("wsname"), boost::algorithm::is_any_of(";"));
  boost::algorithm::split(model_names, options.get<std::string>("weightfile"), boost::algorithm::is_any_of(";"));
  assertm((infile_names.size() == dataset_names.size() && dataset_names.size() == ws_names.size() && ws_names.size() == model_names.size()),
          "Input file names, dataset names, model names and workspace names need to have the same size");
  //let's also check here already if our config file looks like we expect it to
  auto const samplenode = configtree.get_child("samples");
  msgsvc.debugmsg("size of samplenode: " + std::to_string(samplenode.size()));
  assertm((infile_names.size() == samplenode.size()), "\"samples\" node in config tree should have the same size as the input datasets");

  ///////////////////////////////////////////
  ///  setup observable, canvas, plot...  ///
  ///////////////////////////////////////////
  // set basic plot style
  lhcbStyle(static_cast<int>(msgsvc.GetMsgLvl()));

  const float textsize = configtree.get<float>("textsize", gStyle->GetTextSize());
  const float Left_margin = configtree.get<float>("lmargin", 0.11);
  const float Top_margin = configtree.get<float>("tmargin", 0.01);
  const float Right_margin = configtree.get<float>("rmargin", 0.01);
  const float Bigy_margin = configtree.get<float>("bmargin", 0.12);
  const float padsplit = configtree.get<float>("pullheight", 0.32);
  const float padsplitmargin = 1e-6;  //leave this hardcoded as it's the only thing making sense in this layout

  //general setting for reasonable perpendicular edges on the error-bars
  gStyle->SetEndErrorSize(configtree.get("EndErrorSize", 3 * gStyle->GetMarkerSize()));

  TCanvas c1("c1", "Fit", configtree.get("CanvXSize", 2048), configtree.get("CanvYSize", 1280));
  TGaxis::SetMaxDigits(configtree.get("MaxDigits", TGaxis::GetMaxDigits()));
  auto set_margins = [](TPad& p, const float& l, const float& b, const float& r, const float& t) {
    p.SetBorderSize(0);
    p.SetLeftMargin(l);
    p.SetBottomMargin(b);
    p.SetRightMargin(r);
    p.SetTopMargin(t);
  };

  TPad pad1("pad1", "pad1", padsplitmargin, padsplit, 1 - padsplitmargin, 1 - padsplitmargin);
  pad1.Draw();
  pad1.cd();
  set_margins(pad1, Left_margin, padsplitmargin, Right_margin, Top_margin);
  pad1.SetTickx();
  pad1.SetTicky();
  if (configtree.get_optional<bool>("logy")) pad1.SetLogy();
  c1.Update();

  //finding stuff like this costs some nerves. at least it brought me to use gdb
  RooPlot::setAddDirectoryStatus(false);
  //get observable and make frame
  const auto obsn = configtree.get<std::string>("var");
  msgsvc.infomsg("Making plot of " + obsn);
  //hopefully wlog (configure carefully!) we're taking the observable from the first ws
  const auto Observable = IOjuggler::get_wsobj<RooRealVar>(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(infile_names[0], wd), ws_names[0]), obsn);
  const auto nbins = configtree.get<int>("bins");
  auto plotframe = Observable->frame(configtree.get<double>("low"), configtree.get<double>("high"), nbins);

  TPad pad2("pad2", "pad2", padsplitmargin, padsplitmargin, 1 - padsplitmargin, padsplit);
  set_margins(pad2, Left_margin, (Bigy_margin / padsplit), Right_margin, padsplitmargin);
  pad2.SetTickx();
  pad2.SetTicky();

  auto pullframe = Observable->frame(configtree.get<double>("low"), configtree.get<double>("high"), nbins);
  pullframe->SetTitle(" ");
  if (configtree.get("HideExpX", 0))
    TGaxis::SetExponentOffset(1e+9, 1e+9, "x");  //offset = pad size * 1e+7
  pullframe->GetXaxis()->SetTitleOffset(configtree.get("xOffset", 1.05));
  pullframe->GetXaxis()->SetTitleSize(textsize / padsplit);
  pullframe->GetXaxis()->SetLabelSize(textsize / padsplit);
  pullframe->GetXaxis()->SetTitle(configtree.get("xtitle", "M").data());
  pullframe->GetXaxis()->SetTickLength(plotframe->GetXaxis()->GetTickLength() / (padsplit / (1 - padsplit)));
  pullframe->GetXaxis()->SetNdivisions(configtree.get("NdivX", gStyle->GetNdivisions("X")));
  pullframe->GetYaxis()->SetLabelSize(textsize / padsplit);
  pullframe->GetYaxis()->SetTitleSize(textsize / padsplit);
  pullframe->GetYaxis()->SetNdivisions(configtree.get("NdivPY", 3));
  pullframe->GetYaxis()->SetTitleOffset(configtree.get("yOffset", 1.2) * padsplit);
  pullframe->GetYaxis()->SetTitle(configtree.get("ytitlePull", "Pull ").data());
  pullframe->GetYaxis()->SetRangeUser(-configtree.get("PullRange", 4.8),
                                      configtree.get("PullRange", 4.8));

  //when working with linked lists in RooFit setProcessRecArgs(true,false); has to be called
  auto add_args_to_llist = [](RooLinkedList& llist, std::vector<RooCmdArg*>& args) {
    for (auto arg : args) {
      arg->setProcessRecArgs(true, false);
      llist.Add(arg);
    }
  };
  auto del_args = [](std::vector<RooCmdArg*>&& args) {
    for (auto arg : args)
      delete arg;
    args.clear();
  };

  //////////////////////////////////////
  ///  huge lambda for the plotting  ///
  //////////////////////////////////////
  auto plot_multiple_datasets = [&c1, &plotframe, &pullframe, &msgsvc, &add_args_to_llist, &del_args, &nbins](const pt::ptree& plotnode, const std::string& samplename, const RooWorkspace* w, const RooDataSet* ds, const RooAbsPdf* modelPdf) -> void {
    //we might need a category name. set a default one if nothing is found in the config file
    const auto catn = plotnode.get("category", "category");
    //a constant scaling factor for data and pdfs
    const auto scalefactor = plotnode.get_optional<double>("Scale");
    ///////////////////
    ///  plot data  ///
    ///////////////////
    //init lists for plotting data
    RooLinkedList dpl;
    std::vector<RooCmdArg*> dpargs;
    dpargs.push_back(new RooCmdArg(RooFit::MarkerSize(plotnode.get("MSizeData", gStyle->GetMarkerSize()))));
    dpargs.push_back(new RooCmdArg(RooFit::MarkerStyle(plotnode.get("MStyleData", gStyle->GetMarkerStyle()))));
    dpargs.push_back(new RooCmdArg(RooFit::MarkerColor(LHCb::color(plotnode.get("MColorData", "black")))));
    dpargs.push_back(new RooCmdArg(RooFit::LineWidth(plotnode.get("LWidthData", gStyle->GetLineWidth()))));
    dpargs.push_back(new RooCmdArg(RooFit::LineColor(LHCb::color(plotnode.get("LColorData", "black")))));
    dpargs.push_back(new RooCmdArg(RooFit::DrawOption(plotnode.get("OptionData", "e1p").data())));
    dpargs.push_back(new RooCmdArg(RooFit::DataError(static_cast<RooAbsData::ErrorType>(plotnode.get<int>("DataError", 3)))));
    if (scalefactor) dpargs.push_back(new RooCmdArg(RooFit::Rescale(*scalefactor)));
    if (const auto cts = plotnode.get_optional<std::string>("Cut"))
      dpargs.push_back(new RooCmdArg(RooFit::Cut((catn + "==" + catn + "::" + *cts).data())));
    add_args_to_llist(dpl, dpargs);
    ds->plotOn(plotframe, dpl);

    ////////////////////////////////
    ///  plot model projections  ///
    ////////////////////////////////
    ///  automated plotting  ///
    ////////////////////////////
    //we need these two optional childs here and later --> store them
    const auto& sg = plotnode.get_child_optional("stacked_graphs");
    const auto& graphs = plotnode.get_child_optional("graphs");
    if (!sg && !graphs) {
      //put direct daughters of the full model into a container
      std::vector<RooAbsPdf*> daughter_pdfs;
      auto ita = modelPdf->serverMIterator();
      while (auto pdf = ita.next())
        if (pdf->InheritsFrom("RooAbsPdf"))
          daughter_pdfs.push_back(static_cast<RooAbsPdf*>(pdf));
      auto ndau = daughter_pdfs.size();
      msgsvc.debugmsg("Starting automated plotting with " + std::to_string(ndau + 1) + " components");
      //setup colors
      if (auto pltix = plotnode.get_optional<int>("Palette")) {
        if (*pltix >= 113) {
          try {
            set_CustomPalette(*pltix);
          } catch (std::exception& e) {
            msgsvc.warningmsg("caught exception: " + std::string(e.what()));
            msgsvc.warningmsg("Setting Palette 56 to be able to continue");
            gStyle->SetPalette(56);
          }
        } else
          gStyle->SetPalette(*pltix);
      }
      const int ndivz = gStyle->GetNumberContours();
      const int ncolors = gStyle->GetNumberOfColors();
      const double zscale = ndivz / ndau;
      auto GetColorFromIndex = [&ncolors, &ndivz, &zscale](float idx) -> int {
        int color = int(0.01 + idx * zscale);
        int theColor = int((color + 0.99) * double(ncolors) / double(ndivz));
        if (theColor > ncolors - 1) theColor = ncolors - 1;
        return gStyle->GetColorPalette(theColor);
      };

      std::vector<std::string> adds_to(ndau - 1u, "");  //container to store names of graphs to which the currently iterated will be added
      unsigned int stack_counter = 0;
      //plot all components as invisible (to be able to stack later)
      auto stlvit = --std::end(daughter_pdfs);  //damn iterators... there's probably a nice solution in boost
      for (auto comp = std::begin(daughter_pdfs); comp != stlvit; ++comp) {
        std::string comp_name = (*comp)->GetName();
        msgsvc.debugmsg("Adding " + comp_name + " as invisible component to the plot");

        RooLinkedList invll;
        std::vector<RooCmdArg*> args;
        args.push_back(new RooCmdArg(RooFit::Invisible()));
        args.push_back(new RooCmdArg(RooFit::Components(comp_name.data())));
        adds_to[stack_counter] = samplename + comp_name + "P";
        args.push_back(new RooCmdArg(RooFit::Name(adds_to[stack_counter].data())));
        if (stack_counter > 0)
          args.push_back(new RooCmdArg(RooFit::AddTo(adds_to[stack_counter - 1].data(), 1, 1)));
        //at this point, stuff for simultaneous plotting would appear, but i'm not sure if it can be automated easily...

        add_args_to_llist(invll, args);
        modelPdf->plotOn(plotframe, invll);
        del_args(std::move(args));
        stack_counter++;
      }
      //plot visible components from top to bottom
      for (auto comp = boost::rbegin(daughter_pdfs); comp != boost::rend(daughter_pdfs); ++comp) {
        std::string comp_name = (*comp)->GetName();
        RooLinkedList ll;
        std::vector<RooCmdArg*> args;
        args.push_back(new RooCmdArg(RooFit::Components(comp_name.data())));
        args.push_back(new RooCmdArg(RooFit::Name((samplename + comp_name + "P").data())));
        args.push_back(new RooCmdArg(RooFit::LineColor(GetColorFromIndex(stack_counter))));
        args.push_back(new RooCmdArg(RooFit::LineStyle(1)));
        args.push_back(new RooCmdArg(RooFit::LineWidth(2)));
        args.push_back(new RooCmdArg(RooFit::FillColor(GetColorFromIndex(stack_counter))));
        args.push_back(new RooCmdArg(RooFit::DrawOption("F")));
        if (scalefactor) args.push_back(new RooCmdArg(RooFit::Normalization(*scalefactor, RooAbsReal::ScaleType::Relative)));
        stack_counter--;  //at the last component, this will be "0u-1u"
        if (stack_counter < ndau)
          args.push_back(new RooCmdArg(RooFit::AddTo(adds_to[stack_counter].data(), 1, 1)));

        add_args_to_llist(ll, args);
        modelPdf->plotOn(plotframe, ll);
        del_args(std::move(args));
        //manual VLines as default option
        auto gr = static_cast<TGraph*>(plotframe->findObject((samplename + comp_name + "P").data()));
        double gx = 0, gy = 0;
        gr->GetPoint(gr->GetN() - 1, gx, gy);
        //The problem only is, that the last point is not set to 0
        gr->SetPoint(gr->GetN() - 1, gx, 0);
#if ROOT_VERSION_CODE > ROOT_VERSION(6, 19, 0)
        gr->GetPoint(0, gx, gy);
        gr->SetPoint(0, gx, 0);
#endif
      }

      //plot the full model as medium thick blue line
      //this needs to be plotted last, so that the pulls make sense
      modelPdf->plotOn(plotframe, RooFit::Name((static_cast<std::string>(samplename + modelPdf->GetName()) + "P").data()),
                       RooFit::LineWidth(2), RooFit::LineColor(857));
      msgsvc.debugmsg("Finished automated plotting with " + std::to_string(ndau + 1) + " components");
    }

    //some terrible hacks for simultaneous plotting
    auto simultaneous_horror = [&w, &ds, &catn, &scalefactor](auto& args, const auto&& sla, const auto&& nrm, const auto&& pwd) -> bool {
      if (sla || nrm || pwd) {  //ok, we're doomed
        const auto cat = IOjuggler::get_wsobj<RooCategory>(w, catn);
        if (sla)
          args.push_back(new RooCmdArg(RooFit::Slice(*cat, sla->data())));
        if (nrm) {
          //calculate the normalization by hand...
          //sometimes there are components which share their parameters, but have composite normalization like
          // Bkgs::SUM(c1*BKG1,...,BKGn)
          // model::SUM(NSig*SIG,NBkg*Bkgs)
          //in this example c1 is split, and BKG1 only has shared parameters, wo that we can't use Slice.
          //The Normalization of this would be c1_CAT*NBkg_CAT. The code below fiddels this apart and calculates the correct Norm
          double fnorm = 0.0;
          auto titer = cat->typeIterator();
          std::vector<std::string> norms, parnames_without_cat;
          boost::algorithm::split(norms, *nrm, boost::algorithm::is_any_of("*"));
          for (const auto& norm : norms) {
            while (const auto vcat = titer->Next())
              if (boost::algorithm::ends_with(norm, static_cast<std::string>(vcat->GetName())))
                parnames_without_cat.push_back(boost::algorithm::replace_last_copy(norm, static_cast<std::string>(vcat->GetName()), ""));
            titer->Reset();
          }
          while (const auto vcat = titer->Next()) {
            double helper = 1.;
            for (const auto& pwc : parnames_without_cat)
              helper *= IOjuggler::get_wsobj<RooRealVar>(w, pwc + static_cast<std::string>(vcat->GetName()))->getVal();
            fnorm += helper;
          }
          double normalization_for_current_cat = 1.;
          for (const auto& norm : norms)
            normalization_for_current_cat *= IOjuggler::get_wsobj<RooRealVar>(w, norm)->getVal();
          args.push_back(new RooCmdArg(RooFit::Normalization(normalization_for_current_cat / fnorm * (scalefactor ? *scalefactor : 1.), RooAbsPdf::Relative)));
        }
        return true;
      }
      return false;
    };

    ////////////////////////
    ///  stacked graphs  ///
    //add pdf projections. get set of nodes to check if they exist
    RooArgSet branchNodeSet;
    modelPdf->branchNodeServerList(&branchNodeSet);
    //for quick and easy plotting of stacked components
    if (sg) {
      auto nsg = sg->size();
      std::vector<std::string> adds_to(nsg - 1u, "");  //container to store names of graphs to which the currently iterated will be added
      unsigned int stack_counter = 0;
      //plot all components as invisible (to be able to stack later)
      auto stlvit = --std::end(*sg);  //damn iterators... there's probably a nice solution in boost
      for (auto vit = std::begin(*sg); vit != stlvit; ++vit) {
        const auto comp_name = vit->second.get("components", vit->first);
        msgsvc.debugmsg("Adding " + comp_name + " as invisible component to the plot");
        if (branchNodeSet.find(comp_name.data()) == nullptr) {
          msgsvc.debugmsg("The component " + comp_name + " is not part of the model PDF and can't be plotted");
          continue;
        }
        RooLinkedList invll;
        std::vector<RooCmdArg*> args;
        args.push_back(new RooCmdArg(RooFit::Invisible()));
        args.push_back(new RooCmdArg(RooFit::Components(comp_name.data())));  //the component has to be given!
        adds_to[stack_counter] = samplename + comp_name + "P";
        args.push_back(new RooCmdArg(RooFit::Name(adds_to[stack_counter].data())));
        if (stack_counter > 0)
          args.push_back(new RooCmdArg(RooFit::AddTo(adds_to[stack_counter - 1].data(), 1, 1)));
        //putting the common stuff into a lambda results in seg-faults... so you'll find this part below it tree times
        if (simultaneous_horror(args, vit->second.get_optional<std::string>("Slice"), vit->second.get_optional<std::string>("Normalization"), vit->second.get("ProjWData", 0)))
          args.push_back(new RooCmdArg(RooFit::ProjWData(*IOjuggler::get_wsobj<RooCategory>(w, catn), *ds)));  //putting this into the lambda results in seg-fault
        add_args_to_llist(invll, args);
        modelPdf->plotOn(plotframe, invll);
        del_args(std::move(args));
        stack_counter++;
      }
      //plot visible components from top to bottom
      for (auto vit = boost::rbegin(*sg); vit != boost::rend(*sg); ++vit) {
        const auto comp_name = vit->second.get("components", vit->first);
        msgsvc.debugmsg("Now plotting " + comp_name);
        if (branchNodeSet.find(comp_name.data()) == nullptr) {
          msgsvc.debugmsg("The component " + comp_name + " is not part of the model PDF and can't be plotted");
          continue;
        }
        RooLinkedList ll;
        std::vector<RooCmdArg*> args;
        args.push_back(new RooCmdArg(RooFit::Components(comp_name.data())));
        args.push_back(new RooCmdArg(RooFit::Name((samplename + comp_name + "P").data())));
        args.push_back(new RooCmdArg(RooFit::LineColor(LHCb::color((*vit).second.get("linecolor", "blue")))));  //set color to blue if not specified
        args.push_back(new RooCmdArg(RooFit::LineStyle(vit->second.get("style", 1))));
        args.push_back(new RooCmdArg(RooFit::LineWidth(vit->second.get("linewidth", 3))));
        args.push_back(new RooCmdArg(RooFit::FillColor(LHCb::color((*vit).second.get("fillcolor", "blue")))));
        args.push_back(new RooCmdArg(RooFit::DrawOption(vit->second.get("option", "L").data())));
        if (scalefactor) args.push_back(new RooCmdArg(RooFit::Normalization(*scalefactor, RooAbsReal::ScaleType::Relative)));
        stack_counter--;  //at the last component, this will be "0u-1u"
        if (stack_counter < nsg)
          args.push_back(new RooCmdArg(RooFit::AddTo(adds_to[stack_counter].data(), 1, 1)));
        if (simultaneous_horror(args, vit->second.get_optional<std::string>("Slice"), vit->second.get_optional<std::string>("Normalization"), vit->second.get("ProjWData", 0)))
          args.push_back(new RooCmdArg(RooFit::ProjWData(*IOjuggler::get_wsobj<RooCategory>(w, catn), *ds)));  //putting this into the lambda results in seg-fault
        add_args_to_llist(ll, args);
        modelPdf->plotOn(plotframe, ll);
        del_args(std::move(args));
        //manual VLines as default option
        auto gr = static_cast<TGraph*>(plotframe->findObject((samplename + comp_name + "P").data()));
        double gx = 0, gy = 0;
        gr->GetPoint(gr->GetN() - 1, gx, gy);
        //The problem only is, that the last point is not set to 0
        gr->SetPoint(gr->GetN() - 1, gx, 0);
#if ROOT_VERSION_CODE > ROOT_VERSION(6, 19, 0)
        gr->GetPoint(0, gx, gy);
        gr->SetPoint(0, gx, 0);
#endif
        if( (*vit).second.get_optional<float>("linecolor_alpha") || (*vit).second.get_optional<float>("fillcolor_alpha") ){
          gr->SetLineColorAlpha(LHCb::color((*vit).second.get("linecolor", "blue")), (*vit).second.get("linecolor_alpha",1.f));
          gr->SetFillColorAlpha(LHCb::color((*vit).second.get("fillcolor", "blue")), (*vit).second.get("fillcolor_alpha",1.f));
        }
      }
    }

    ////////////////////////////
    ///  non-stacked graphs  ///
    if (graphs) {
      for (const auto& v : *graphs) {
        //check if component exists (no check for type yet, but this should be a corner case for our purpose)
        const auto comp_name = v.second.get("components", v.first);
        msgsvc.debugmsg("Now plotting " + comp_name);
        if (branchNodeSet.find(comp_name.data()) == nullptr) {
          msgsvc.debugmsg("The component " + comp_name + " is not part of the model PDF and can't be plotted");
          continue;
        }
        RooLinkedList ledlist;
        std::vector<RooCmdArg*> args;
        if (v.second.get_optional<bool>("vlines")) args.push_back(new RooCmdArg(RooFit::VLines()));
        if (v.second.get_optional<bool>("invisible")) args.push_back(new RooCmdArg(RooFit::Invisible()));
        args.push_back(new RooCmdArg(RooFit::Components(comp_name.data())));  //the component has to be given!
        args.push_back(new RooCmdArg(RooFit::Name((samplename + comp_name + "P").data())));
        args.push_back(new RooCmdArg(RooFit::LineColor(LHCb::color(v.second.get("linecolor", "blue")))));  //set color to blue if not specified
        args.push_back(new RooCmdArg(RooFit::LineStyle(v.second.get("style", 1))));
        args.push_back(new RooCmdArg(RooFit::LineWidth(v.second.get("linewidth", 3))));
        args.push_back(new RooCmdArg(RooFit::FillColor(LHCb::color(v.second.get("fillcolor", "blue")))));
        args.push_back(new RooCmdArg(RooFit::DrawOption(v.second.get("option", "L").data())));
        args.push_back(new RooCmdArg(RooFit::AddTo(v.second.get("addto", "").data(), 1, 1)));
        if (scalefactor) args.push_back(new RooCmdArg(RooFit::Normalization(*scalefactor, RooAbsReal::ScaleType::Relative)));
        if (simultaneous_horror(args, v.second.get_optional<std::string>("Slice"), v.second.get_optional<std::string>("Normalization"), v.second.get("ProjWData", 0)))
          args.push_back(new RooCmdArg(RooFit::ProjWData(*IOjuggler::get_wsobj<RooCategory>(w, catn), *ds)));  //putting this into the lambda results in seg-fault
        add_args_to_llist(ledlist, args);
        modelPdf->plotOn(plotframe, ledlist);
        del_args(std::move(args));

        //as RooFit::VLines() doesn't do what it's supposed to, let's try something else
        if (v.second.get_optional<bool>("manual_vlines")) {
          auto gr = static_cast<TGraph*>(plotframe->findObject((samplename + v.second.get<std::string>("components") + "P").data()));
          double gx = 0, gy = 0;
          gr->GetPoint(gr->GetN() - 1, gx, gy);
          //The problem only is, that the last point is not set to 0
          gr->SetPoint(gr->GetN() - 1, gx, 0);
        }
      }
    }

    //draw data again since pdf projections may overlay parts of the data
    dpargs.push_back(new RooCmdArg(RooFit::Name((samplename + "P").data())));
    dpargs.back()->setProcessRecArgs(true, false);
    dpl.Add(dpargs.back());
    ds->plotOn(plotframe, dpl);
    del_args(std::move(dpargs));

    /////////////////////////////////
    ///  add pulls to plottables  ///
    /////////////////////////////////
    // Construct a histogram with the pulls of the data w.r.t the curve
    auto hpull = plotframe->pullHist();  //heap https://root.cern.ch/doc/master/RooHist_8cxx_source.html#l00705
    hpull->SetMarkerSize(plotnode.get("MSizePull", gStyle->GetMarkerSize()));
    hpull->SetMarkerColor(LHCb::color(plotnode.get("MColorPull", "azure+3")));
    hpull->SetLineWidth(plotnode.get("LWidthPull", gStyle->GetLineWidth()));
    hpull->SetLineColor(LHCb::color(plotnode.get("LColorPull", "azure+3")));
    hpull->SetFillColorAlpha(LHCb::color(plotnode.get("FColorPull", "azure+3")),plotnode.get("FAlphaPull",1.f));
    hpull->SetDrawOption(plotnode.get("OptionPull", "e1p").data());

    // Create a new frame to draw the pull distribution and add the distribution to the frame
    pullframe->addPlotable(hpull, plotnode.get("OptionPull", "e1p").data());

    //////////////////////////////////////
    ///  convergence check, runs test  ///
    //////////////////////////////////////
    //Small convergence check
    if (plotnode.get("PullCC", 1)) {
      auto bins_gt_threshold = 0;
      //pulls should be within "pull_threshold" sigma
      const auto pull_threshold = plotnode.get("PullThreshold", 4.0);
      bool wawo = hpull->GetY()[0] > 0;
      unsigned int npos = wawo ? 1u : 0u, nneg = wawo ? 0u : 1u;
      unsigned int wawo_counter = 1u;
      for (auto ij = 0u; ij < nbins; ij++) {
        auto pullbinval = hpull->GetY()[ij];
        if (ij > 0 && wawo != (pullbinval > 0)) {
          wawo_counter++;
          wawo = pullbinval > 0;
        }
        wawo ? npos++ : nneg++;
        if (std::fabs(pullbinval) > pull_threshold) bins_gt_threshold++;
      }
      //number of bins that are allowed to be bigger than pull_threshold
      if (bins_gt_threshold >= plotnode.get("PullBGT", 2))
        msgsvc.warningmsg("\033[0;31mPulls too high! Please have a look\033[0m");
      const auto expected_mean_wawo = static_cast<float>(2 * npos * nneg) / static_cast<float>(nbins) + 1.;
      const auto variance_wawo = (expected_mean_wawo - 1.) * (expected_mean_wawo - 2.) / (static_cast<float>(nbins - 1.));
      const auto nsigma_wawo = (wawo_counter - expected_mean_wawo) / std::sqrt(variance_wawo);
      msgsvc.infomsg(TString::Format("Wald-Wolfowitz p-value of pulls: %.4f , (%.2f sigma)", ROOT::Math::normal_cdf_c(std::fabs(nsigma_wawo)), nsigma_wawo));
    }
    return;
  };

  //////////////////////////////////////////////////////
  ///  loop over inputs calling the plotting lambda  ///
  //////////////////////////////////////////////////////
  unsigned int plot_counter = 0u;
  //catch the shit that goes out of scope
  std::vector<RooWorkspace*> workspacesthatwouldgooutofscopeifidontdothisshithere(samplenode.size());
  std::vector<RooDataSet*> datasetsthatwouldgooutofscopeifidontdothisshithere(samplenode.size());
  std::vector<RooAbsPdf*> modelsthatwouldgooutofscopeifidontdothisshithere(samplenode.size());
  for (const auto& plotnode : samplenode) {
    ////////////////////////////
    ///  get data and model  ///
    ////////////////////////////
    const auto w(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(infile_names[plot_counter], wd), ws_names[plot_counter]));
    const auto DATA(IOjuggler::get_wsobj<RooDataSet>(w, dataset_names[plot_counter]));
    const auto PDF(IOjuggler::get_wsobj<RooAbsPdf>(w, model_names[plot_counter]));
    // make the damn plot
    plot_multiple_datasets(plotnode.second, plotnode.first, w, DATA, PDF);
    plot_counter++;
    workspacesthatwouldgooutofscopeifidontdothisshithere[plot_counter] = std::move(w);
    datasetsthatwouldgooutofscopeifidontdothisshithere[plot_counter] = std::move(DATA);
    modelsthatwouldgooutofscopeifidontdothisshithere[plot_counter] = std::move(PDF);
  }

  ////////////////////////////////////////
  ///  axis manipulation, fine tuning  ///
  ////////////////////////////////////////
  //some axis-modifications
  plotframe->GetXaxis()->SetNoExponent();    //<-- spoils MaxDigits settings, so don't use it on other axis
  plotframe->GetXaxis()->SetLabelSize(0.0);  //don't print labels
  plotframe->GetXaxis()->SetTickLength(configtree.get("TickLengthX", plotframe->GetXaxis()->GetTickLength()) / (1 - padsplit));
  plotframe->GetXaxis()->SetNdivisions(configtree.get("NdivX", gStyle->GetNdivisions("X")));
  if (configtree.get("HideExpY", 0))
    TGaxis::SetExponentOffset(1e+9, 1e+9, "y");  //offset = pad size * 1e+7
  plotframe->GetYaxis()->SetTickLength(configtree.get("TickLengthY", plotframe->GetYaxis()->GetTickLength()));
  plotframe->GetYaxis()->SetTitleSize(textsize / (1 - padsplit));
  plotframe->GetYaxis()->SetLabelSize(textsize / (1 - padsplit));
  plotframe->GetYaxis()->SetTitleOffset(configtree.get("yOffset", 1.2) * (1 - padsplit));
  const auto parsed_ymax = configtree.get("ymax", plotframe->GetMaximum() + configtree.get("ymaxadd", 0.f) * (plotframe->GetMaximum() - configtree.get("ymin", 0.1)));
  plotframe->GetYaxis()->SetRangeUser(configtree.get("ymin", 0.1), parsed_ymax);
  const auto binsize = (configtree.get<double>("high") - configtree.get<double>("low")) / nbins;
  plotframe->GetYaxis()->SetTitle(configtree.get("ytitle", std::fabs(std::log10(parsed_ymax)) >= configtree.get("MaxDigits", TGaxis::GetMaxDigits()) ? TString::Format("10^{%d} Events/%.3g MeV", static_cast<int>(3 * std::ceil((std::log10(parsed_ymax) / 3.0))), binsize).Data() : TString::Format("Events/%.3g MeV", binsize).Data()).data());
  plotframe->GetYaxis()->SetNdivisions(configtree.get("NdivY", gStyle->GetNdivisions("Y")));
  plotframe->SetTitle("");
  plotframe->Draw();

  //pulls: go back to canvas and start using the second pad
  c1.cd();
  pad2.Draw();
  pad2.cd();
  std::vector<std::unique_ptr<TLine> > plines;
  if (const auto pl = configtree.get_child_optional("pullLines"))
    for (const auto& l : *pl) {
      std::unique_ptr<TLine> pullline(new TLine(configtree.get<double>("low"), l.second.get<double>("pull"),
                                                configtree.get<double>("high"), l.second.get<double>("pull")));
      pullline->SetLineStyle(l.second.get("style", 2));
      pullline->SetLineWidth(l.second.get("width", gStyle->GetLineWidth()));
      pullline->SetLineColor(LHCb::color(l.second.get("color", "black")));
      pullline->SetDrawOption(l.second.get("option", "l").data());
      plines.push_back(std::move(pullline));
    }
  for (const auto& line : plines) pullframe->addObject(line.get());
  pullframe->Draw();
  //////////////////
  ///  legends   ///
  //////////////////
  c1.cd();
  //add optional legends
  //need to store them in a vector since the legends in the loop go out of scope and cause seg-fault when frame->Draw() is called
  std::vector<std::unique_ptr<TLegend> > legs;
  if (const auto opt_legs = configtree.get_child_optional("legends")) {
    for (const auto& l : *opt_legs) {
      const auto legxlo = l.second.get("xlo", 0.0);
      const auto legylo = l.second.get_optional<double>("dtop") && l.second.get_optional<double>("height") ? 1 - l.second.get<double>("dtop") - (l.second.get<double>("height") * textsize) : l.second.get("ylo", 0.0);
      const auto legxhi = l.second.get_optional<double>("dright") ? 1 - l.second.get<double>("dright") : l.second.get("xhi", 1.0);
      const auto legyhi = l.second.get_optional<double>("dtop") ? 1 - l.second.get<double>("dtop") : l.second.get("yhi", 1.0);
      std::unique_ptr<TLegend> leg(new TLegend(legxlo, legylo, legxhi, legyhi, static_cast<std::string>(l.second.get("header", "")).data(), "brNDC"));
      leg->SetBorderSize(0);
      leg->SetFillColor(kWhite);
      leg->SetTextAlign(12);
      leg->SetFillStyle(0);
      leg->SetMargin(l.second.get("Margin", leg->GetMargin()));
      leg->SetTextSize(l.second.get<double>("scale", 1.f) * textsize);
      for (const auto& v : l.second) {
        if (const auto lbl = v.second.get_optional<std::string>("label")) {
          const auto labn = v.second.get("name", v.first + "P");
          auto grp = static_cast<TGraph*>(plotframe->findObject(labn.data()));
          if (grp == nullptr)
            msgsvc.debugmsg("Cannot find " + labn + " in current plot. Skipping entry");
          else{
            // hack to manipulate legend entries
            if(v.second.get_optional<float>("MSize") || v.second.get_optional<float>("LWidth")){
              auto clone = static_cast<TGraph*>(grp->Clone((static_cast<std::string>(grp->GetName())+"_dumb_clone").data()));
              clone->SetLineWidth(v.second.get("LWidth",grp->GetLineWidth()));
              clone->SetMarkerSize(v.second.get("MSize",grp->GetMarkerSize()));
              leg->AddEntry(clone, (*lbl).data(), v.second.get("option", "lpf").data());
            }
            else leg->AddEntry(grp, (*lbl).data(), v.second.get("option", "lpf").data());
          }
        }
      }
      legs.push_back(std::move(leg));
    }
  }
  for (const auto& leg : legs)
    leg.get()->Draw();

  ///////////////////////////
  ///  save plot to disk  ///
  ///////////////////////////
  TString fname = static_cast<TString>(boost::algorithm::replace_all_copy(ofn, ".root", ""));
  //strip prepended path and add observable name
  auto autogen_plotname = static_cast<std::string>(TString::Format("%s_%s", static_cast<TObjString*>(fname.Tokenize("/")->Last())->String().Data(), Observable->GetName()).Data());
  auto plotname = configtree.get("plotname", autogen_plotname);

  //check if plotname contains a directory and if that exists
  IOjuggler::dir_exists(wd + "/" + plotname + ".bla");
  auto tmpeil = gErrorIgnoreLevel;
  if (msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    gErrorIgnoreLevel = kInfo;  //i still want to know where the plots have been saved
  for (const auto& ex : configtree.get_child("outputformats"))
    c1.SaveAs((wd + "/" + plotname + "." + ex.first).data());
  gErrorIgnoreLevel = tmpeil;

  return 0;
}