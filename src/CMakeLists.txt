#///////////////////////////////////////////////////////////////////////////
#//
#//    Copyright 2015
#//
#///////////////////////////////////////////////////////////////////////////
#//-------------------------------------------------------------------------
#//
#// Description:
#//      build file for the fitter
#//
#//
#// Author List:
#//      Sebastian Neubert    Heidelberg University       (original author)
#//      Marian Stahl         Heidelberg University
#//
#//-------------------------------------------------------------------------


message(STATUS "")
message(STATUS "${BoldWhite}>>> Setting up 'beef/src' directory.${ColourReset}")

if(NOT ROOT_FOUND)
  message(FATAL_ERROR "ROOT was not found. Aborting...")
endif()

if(NOT Boost_FOUND)
  message(FATAL_ERROR "Boost was not found. Aborting...")
endif()

#get git hash to be used for printing in executables
if(GIT_FOUND)
  execute_process(
    COMMAND ${GIT_EXECUTABLE} --git-dir=${CMAKE_CURRENT_SOURCE_DIR}/../.git rev-parse HEAD
    OUTPUT_VARIABLE BEEF_GIT_HASH
    RESULT_VARIABLE _BEEF_GIT_LOG_RETURN
    OUTPUT_STRIP_TRAILING_WHITESPACE
  )
  if(NOT _BEEF_GIT_LOG_RETURN)
    message(STATUS "beef's git repository hash is '${BEEF_GIT_HASH}'.")
    add_definitions(-DBEEF_GIT_HASH=\"${BEEF_GIT_HASH}\")
  else()
    message(STATUS "Error running 'git'. Repository hash unknown.")
  endif()
  execute_process(
    COMMAND ${GIT_EXECUTABLE} --git-dir=${CMAKE_CURRENT_SOURCE_DIR}/../IOjuggler/.git rev-parse HEAD
    OUTPUT_VARIABLE IOJ_GIT_HASH
    RESULT_VARIABLE _IOJ_GIT_LOG_RETURN
    OUTPUT_STRIP_TRAILING_WHITESPACE
  )
  if(NOT _IOJ_GIT_LOG_RETURN)
    message(STATUS "IOjuggler's git repository hash is '${IOJ_GIT_HASH}'.")
    add_definitions(-DIOJ_GIT_HASH=\"${IOJ_GIT_HASH}\")
  else()
    message(STATUS "Error running 'git'. Repository hash unknown.")
  endif()
endif()

# set include directories (picks up local version of IOjuggler)
set(BEEF_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}  ${CMAKE_CURRENT_SOURCE_DIR}/../include ${CMAKE_CURRENT_SOURCE_DIR}/../IOjuggler)

#custom dictionaries and libraries
set(custom_pdfs RooRelBreitWigner RooHILLdini RooHORNSdini RooTiltedSubbotin RooPRBox RooPRHorns RooPRHill)

#build and link executables
foreach(cpdf ${custom_pdfs})
  list(APPEND cpdfheaders ${CMAKE_CURRENT_SOURCE_DIR}/../include/${cpdf}.h)
  list(APPEND cpdfsources ${cpdf}.cc)
endforeach(cpdf)
set(CPDFHEAD "${cpdfheaders}")
set(CPDFSRC  "${cpdfsources}")
message(STATUS "Getting headers for custom pdfs from ${CPDFHEAD}")

#generate dictionary for custom p.d.f.s
set(CMAKE_INSTALL_LIBDIR ${CMAKE_BINARY_DIR}/lib)
ROOT_GENERATE_DICTIONARY(RooCustomDict ${CPDFHEAD} MODULE RooCustomLib LINKDEF ${CMAKE_CURRENT_SOURCE_DIR}/../include/RooCustomDict_LinkDef.h)
add_library(RooCustomLib SHARED ${CPDFSRC} RooCustomDict.cxx)
target_include_directories(RooCustomLib PUBLIC ${BEEF_INCLUDE_DIRS})
target_link_libraries(RooCustomLib PUBLIC ${ROOT_LIBRARIES})

# define list of executables
set(EXECUTABLES beef splot generate_toys approximate_discrete_profiling)
#optional stuff
if(DEFINED BEEF_PLOT)
  list(APPEND EXECUTABLES beef_plot beef_plots)
endif()

#build and link executables
foreach(EXC ${EXECUTABLES})
  add_executable(${EXC} ${EXC}.cpp)
  target_include_directories(${EXC} PRIVATE ${BEEF_INCLUDE_DIRS})
  #link libraries by hand since global linking breaks implicit multithreading
  target_link_libraries(${EXC} ${CMAKE_EXE_LINKER_FLAGS} ${ROOT_LIBRARIES} ROOT::RooFit ROOT::RooFitCore ROOT::MathMore RooCustomLib)
endforeach(EXC)

#Extrawurst
target_link_libraries(beef ROOT::RooStats)
target_link_libraries(splot ROOT::RooStats)